## 55e réunion du groupe de travail

**jeudi 06 janvier 2022 à 11h15**

Personnes présentes : Angie / Cpm / MrFlos

### Divers

   * question de la persistance des compte-rendus de réunions :
       * TODO mrflos : faire une revue des markdown cassés
           * 1 sur 2 fait, à suivre
       * TODO mrflos persister les CR du n°37 au n°54


   * intégration informations portée n°13 :
       * stats nombre de membres
           * FAIT Angie : modifier le fichier chatons.properties avec les stats du nombre de membres et les subs des 6 nouveaux chatons
       * fichiers membres:
           * FAIT Angie : créer les fichiers organizations.properties pour les 7 nouveaux (ah non 6 puisque Libre-service.eu a déjà fait le boulot)


   * divers précédents :
       * création d'un schéma explicitant les subs
           * TODO Flo+Jeey
       * demande d'amélioration de la doc# sur subs.foo (Zatalyz)
           * Voir conversation zatalys sur le forum et lui répondre
           * TODO Flo+Jeey
       * mrflos/Jeey faire un schéma pour le README.md qui montre l'arborescence entre les fichiers properties
           * répondre sur le forum
           * en cours (mais on va privilégier le papier, le paquet yunohost d'excalidraw n'étant pas mûr ;) )


### Revue de [https://stats.chatons.org/](https://stats.chatons.org/) 

   * page CHATONS : 
       * **décision d'afficher par défaut les organisations et services « actifs » (sans enddate ou avec enddate future)**
           * ne pas se contenter de regarder si le enddate est vide, comparer à la date du jour
           * plus tard éventuellement, ajout d'un fonction pour voir les autres aussi "le cimetière des chatons" 😆
           * TODO Cpm
   * page générique d'un chaton : 
       * penser à augmenter le code html avec les informations de properties pour faciliter le futur réagencement  UI/UX
           * TODO Cpm
   * page « Statistiques » (fédération) :
       * ajouter un donuts sur les services de paiement
           * TODO Cpm 
   * un jour peut-être :
       * pouvoir cliquer sur les graphiques pour voir la liste de résultats correspondant
           * par exemple pour les types d'inscription (à un service)
       * donuts sur les pays
           * pouvoir cliquer sur les résultats du camembert pour avoir une liste des chatons par pays
   * pages Uptimes (Federation, Organization, Services)
       * des améliorations à faire
           * TODO Cpm visibilité autres liens
           * TODO mrflos (pour l'été) : bidouiller la page statsuptime pour utiliser les filtres par état en js datatables
       * questions de statut manuel vs statut mesuré (page organization)
           * statut manuel seulement
           * statut mesuré seulement
           * les deux
           * un seul combiné des deux
           * discussion :
               * est-ce que la version manuelle est encore utile ? pertinence du mesuré
               * se poser la question de ce que cherche l'utilisateur
               * cas des statuts manuels « en travaux » ou  « fermé »
               * le statut manuel est plus important que le statut mesuré, respecté l'expression des admins
               * ne surtout pas afficher les deux
               * étudier la conjonction
           * TODO Cpm voir pour une version « combinée » avec bulle informative
   * Flo :
       * peut être avoir dans résumé des moyennes sans graphes, genre du texte « sur 2020 XXX visiteurs uniques, YYY ips différentes »
           * Cpm : une notion de « tendance » ?
           * Cpm : donner exemple ?
           * TODO Flo, à réfléchir l'enrichissement de texte des graphes toujours en TODO  > GT le 20 juillet > décalé
           * TODO Flo, à réfléchir à des cadres de tendances dans « Résumé » toujours en  > GT le 20 juillet > décalé
       * changer les intitulés « Web » et « Spécifique » par « Graphes de visites web » ou plus court « Graphes Web » et « Statistiques propres aux services » ou plus court « Stats des services » ?
           * Cpm : préciser l'intention
           * Flo : expliquer les items du menu type > GT le 20 juillet (annulé cause covid...)  > décalé
           * TODO Cpm : ajouter des bulles
           * TODO Flo : tester le menu métriques auprès de personnes
               * en cours toujours en TODO
           * TODO réfléchir
       * TODO Cpm afficher les champs nom et description des métrics dans les diagrammes
   * site statique vs site dynamique ?
       * différence de besoin entre le Chapril (métrique au jour) et le collectif CHATONS (mérique au mois)
       * supprimer les vues années, semaines et jours ?
       * supprimer les périodes 12 mois, 2020 et 2021 ?
       * site statique :
           * avantages : simplicité de déploiement (est-ce nécessaire ?)
           * inconvénients : prend de la place sur disque
       * pour site dynamique :
           * avantages : plus grande interactivité, plus de liberté fonctionnelles
           * inconvénients : nécessite l'installation d'un serveur d'application Java, du travail pour finaliser
       * avis :
           * Antoine : pas de problème au site dynamique
           * Flo : peut-être finir les fonctionnalités en cours avant de faire une version 2 dynamique avec des optimisations
   * page d'accueil :
       * entête « Mensuelles » préciser que c'est n-1, x2


### Revue générateur de fichiers métrics (statoolinfos)

   * billet forum TODO Cpm FAIT
       * [https://forum.chatons.org/t/generer-des-fichiers-metriques-avec-la-moulinette-statoolinfos/3083](https://forum.chatons.org/t/generer-des-fichiers-metriques-avec-la-moulinette-statoolinfos/3083)
   * amélioration willdcard Kepon ([https://forge.devinsy.fr/devinsy/statoolinfos/issues/1)](https://forge.devinsy.fr/devinsy/statoolinfos/issues/1))
       * TODO Cpm FAIT
   * ajout support Jitsi :
       * TODO Cpm en cours
   * ajout support Mintest
       * TODO Cpm en attente validation nommage métriques
   * ajout support Mumble
       * TODO Cpm FAIT
   * ajout support PrivateBin
       * TODO Cpm en cours
   * metrics http
       * améliorer détection OS TODO Cpm
       * améliorer détection date TODO Cpm
   * question existentielle :
       * corriger visit pour prendre aussi les http erreur


### Revue des catégories

   *  ([https://stats.chatons.org/category-autres.xhtml)](https://stats.chatons.org/category-autres.xhtml)) :
       * RAS


### Revue des fichiers properties de membres

   * passer en revue :
       *  [https://stats.chatons.org/chatons-crawl.xhtml](https://stats.chatons.org/chatons-crawl.xhtml)
           * plus tard
       * [https://stats.chatons.org/chatons-propertyalerts.xhtml](https://stats.chatons.org/chatons-propertyalerts.xhtml)
           * 303 rouges, 1185 jaunes, communiquer un jour prochain (au moment de la prochaine release - celle avec les metrics)
   * metrics spécifiques à chaque service à penser
       * besoin de repasser dessus pour le nommage avant de propager
       * besoin de coder leur affichage pour stats.chatons.org
       * besoin de paramétrer des moulinettes pour les récupérations automatisées de metrics


### Revue des tickets

   * [https://framagit.org/chatons/chatonsinfos/-/issues/1](https://framagit.org/chatons/chatonsinfos/-/issues/1)
       * Redesign des encarts au dessus des tableaux
       * prévu lorsqu'on aura toutes les informations affichées
       * statut : plus tard
   * [https://framagit.org/chatons/chatonsinfos/-/issues/2](https://framagit.org/chatons/chatonsinfos/-/issues/2)
       * Dans le tableau des services de la fiche organisation des chatons, supprimer la colonne "Organisation"
       * Cpm : ce tableau est une vue mutualisée entre plusieurs pages : organisation, services, catégorie,  logiciel ; l'information est effectivement redondante pour la page organisation, mais ça permet de conserver l'homogénéité de la vue.
       * Cpm : pour gagner de la place, possibilité de ne mettre que le logo de l'organisation et le nom en bulle
       * statut : réfléchir et sinon sera traité par la grande revue visuelle prévue un jour
   * [https://framagit.org/chatons/chatonsinfos/-/issues/4](https://framagit.org/chatons/chatonsinfos/-/issues/4)
       * Penser un nouveau fichier properties dédié aux offres non logicielles, celle d'hébergement
       * statut : priorité aux services utilisateurs donc pertinent mais plus tard


### Revue des merge requests

   *  [https://framagit.org/chatons/chatonsinfos/-/merge\_requests](https://framagit.org/chatons/chatonsinfos/-/merge\_requests)
   * [https://framagit.org/chatons/chatonsinfos/-/merge\_requests/40](https://framagit.org/chatons/chatonsinfos/-/merge\_requests/40)
       * Added LibreServiceEU subs entry in chatons.properties
       * TODO Cpm FAIT ;-)


### Revue du forum

   * [https://forum.chatons.org/c/collectif/stats-chatons-org/83](https://forum.chatons.org/c/collectif/stats-chatons-org/83)
   * [https://forum.chatons.org/t/chatonsinfos-version-0-4-en-avant-les-metriques/3082](https://forum.chatons.org/t/chatonsinfos-version-0-4-en-avant-les-metriques/3082)
       * billet forum annonce version 0.4
       * TODO Cpm FAIT
   * [https://forum.chatons.org/t/chatonsinfos-version-0-4-en-avant-les-metriques/3082/2](https://forum.chatons.org/t/chatonsinfos-version-0-4-en-avant-les-metriques/3082/2)
       * question sécurité, données potentiellement compromettantes
       * quelles solution ?
       * TODO Cpm FAIT
   * [https://forum.chatons.org/t/generer-des-fichiers-metriques-avec-la-moulinette-statoolinfos/3083](https://forum.chatons.org/t/generer-des-fichiers-metriques-avec-la-moulinette-statoolinfos/3083)
       * billlet forum annonce moulinette génération métriques
       * TODO Cpm FAIT
   * [https://forum.chatons.org/t/generer-des-fichiers-metriques-avec-la-moulinette-statoolinfos/3083/2](https://forum.chatons.org/t/generer-des-fichiers-metriques-avec-la-moulinette-statoolinfos/3083/2)
       * question Kepon + ticket > amélioration pour gerer le wildcard au début dans les logs
       * TODO Cpm FAIT
   * [https://forum.chatons.org/t/session-d-accompagnement-chatonsinfos-le-mardi-21-12-2021-21h-22h/3089/3](https://forum.chatons.org/t/session-d-accompagnement-chatonsinfos-le-mardi-21-12-2021-21h-22h/3089/3)
       * billet forum accompagnement version 0.4
       * TODO Cpm FAIT
   * [https://forum.chatons.org/t/chatonsinfos-version-0-4-en-avant-les-metriques/3082/4](https://forum.chatons.org/t/chatonsinfos-version-0-4-en-avant-les-metriques/3082/4)
       * Laurent Sleto questions sur les génériques
       * TODO Cpm FAIT
   * [https://forum.chatons.org/t/generer-des-fichiers-metriques-avec-la-moulinette-statoolinfos/3083/6](https://forum.chatons.org/t/generer-des-fichiers-metriques-avec-la-moulinette-statoolinfos/3083/6)
       * Laurent Sleto questions sur la génération ses métriques
       * TODO Cpm FAIT


### Revue des disponibilités des services (uptimes)

   * [https://stats.chatons.org/chatons-uptimes.xhtml](https://stats.chatons.org/chatons-uptimes.xhtml)
   * Nebulae  : c'est réglé ! 
   * Nomagic ->  Hedgedoc : c'est réglé
   * Colibris -> Jitsi  : c'est réglé
   * Automario -> chat XMPP
       * Angie lui a envoyé un mp le 5/01


### Revue de l'ontologie

   * rappel de l'ordre des questions à se poser : préfixe, sous-préfixe
   * métriques HTTP :
       * contexte :
           * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties)
           * [http://www.webalizer.org/webalizer\_help.html](http://www.webalizer.org/webalizer\_help.html)
   * Un jour peut-être : 
       * metrics.ci


   * metrics.pastebins
       * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties#L662](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties#L662)
       * nombre de lecture de paste
       * metrics.pastebins.reads ? ok on le met!
   * rendre générique ip ?
       * contextualisation :
           * concerne tous les services
           * un service n'est pas que http
       * propositions :
           * nombre d'adresses IP visiteuses du service
           * nombre d'adresses IP s'étant connectées au service
           * metrics.service.ip
           * metrics.service.ip.ipv4
           * metrics.service.ip.ipv6
   * étendre le metric ip aux utilisateurs du service ?
       * metrics.service.users.ip
       * metrics.service.users.ip.ipv4
       * metrics.service.users.ip.ipv6
       * nan, pas utile
   * Minetest :
       * metrics.metaverse (Minetest…)
           * metrics.metaverse.worlds vs metrics.metaverse.maps
           * metrics.metaverse.ip -> metrics.service.ip
           * metrics.metaverse.ip.ipv4 -> metrics.service.ip.ipv4
           * metrics.metaverse.ip.ipv6 -> metrics.service.ip.ipv6
           * metrics.metaverse.joins -> metrics.videogames.connections
           * metrics.metaverse.players  -> metrics.service.accounts
           * metrics.metaverse.players.active -> metrics.service.accounts.active + metrics.service.users
       * Métriques non-officielles pour minetest :
           * metrics.minetest.logs
           * metrics.minetest.logs.action
           * metrics.minetest.logs.warning
           * metrics.minetest.logs.error
           * metrics.minetest.logs.unknown
       * utiliser un métrique déjà existant : +1
           * metrics.videogames.games
           * metrics.videogames.gamers
           * metrics.videogames.connections


   * fiche properties hébergement
       * réclamée donc pourquoi ne pas l'envisager
       * après plusieurs tentatives exploratoires, décision de tenter une fichier spécifique
       * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/hosting.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/hosting.properties)
       * cas des vps :
           * vps.os
           * vps.hypervisor
           * vps.
           * la question se pose également pour la section host normale
       * projection d'intégration dans stats.chatons.org
           * besoin d'un bouton « Services dédiés »
           * certainement renommer « Services » en « Services mutualisés »
           * page d'index : 
               * ajouter un nombre spécifique pour les services dédiés
               * ajouter un indicateur pour différencier
               * dans le tableau : un colonne pour les services dédiés / une pour les services mutualisés
           * page d'un membre :
               * ajouter un nombre spécifique pour les services dédiés
               * ajouter un indicateur pour différencier
           * nommage : service mutualisé vs service dédié
           * faire des essais
               * se sont des travaux préparatoires, priorité à l'objectif en cours
               * TODO Angie : créer une fiche "fausse" fiche hosting.properties pour Framasoft afin de tester
                   * [https://framagit.org/framasoft/chatons/-/blob/main/Fichiers\_properties/hosting-nextcloud.properties](https://framagit.org/framasoft/chatons/-/blob/main/Fichiers\_properties/hosting-nextcloud.properties)




## 56e réunion du groupe de travail

**jeudi 20 janvier 2022 à 11h15**

Personnes présentes : Angie / Jeey / Cpm


### Divers

   * question de la persistance des compte-rendus de réunions :
       * TODO mrflos : faire une revue des markdown cassés
           * 1 sur 2 fait, à suivre
       * FAIT mrflos persister les CR du n°37 au n°54


   * Atelier ChatonsInfos le mercredi 19/01
       * 4 personnes de 3 chatons
       * 1 fichier organisation ajouté
       * 1 fichier service ajouté avec métrics


   * divers précédents :
       * création d'un schéma explicitant les subs
           * TODO Flo+Jeey
       * demande d'amélioration de la doc# sur subs.foo (Zatalyz)
           * Voir conversation zatalys sur le forum et lui répondre
           * TODO Flo+Jeey
       * mrflos/Jeey faire un schéma pour le README.md qui montre l'arborescence entre les fichiers properties
           * répondre sur le forum
           * en cours (mais on va privilégier le papier, le paquet yunohost d'excalidraw n'étant pas mûr ;) )


### Revue de [https://stats.chatons.org/](https://stats.chatons.org/) 

   * page CHATONS (accueil) : 
       * entête du tableau « Mensuelles » x2 :
           * trouver le moyen de préciser qu'il s'agit du mois précédent
               * 2 ou 3 lignes max pour l'entête ? plutôt pour 2 lignes
               * possibilités :
                   * 1) mettre dans une bulle le mois concerné
                   * 2) asterisk avec légende en dessous du tableau
                   * 3) Visites / mensuelles*
                   * 4) mention du mois :
                       * Visites / 12/2021
                       * Visites / déc. 2021
           * choix n°3 visible sur [https://infos.libre-service.eu/](https://infos.libre-service.eu/) 
               * TODO Cpm FAIT×
           * bien d'avoir aussi le choix n°1
               * TODO Cpm
       * **décision d'afficher par défaut les organisations et services « actifs » (sans enddate ou avec enddate future)**
           * ne pas se contenter de regarder si le enddate est vide, comparer à la date du jour
           * sont affichés les organizations non AWAY et memberOf(chaton) non AWAY
           * TODO Cpm FAIT
       * plus tard éventuellement, ajout d'un fonction pour voir les autres aussi "le cimetière des chatons" 😆
           * sera facilement faisable avec la version dynamique
           * TODO Cpm
   * page générique d'un chaton : 
       * penser à augmenter le code html avec les informations de properties pour faciliter le futur réagencement  UI/UX
           * TODO Cpm
   * page « Statistiques » (fédération) :
       * ajouter un donuts sur les services de paiement
           * TODO Cpm 
   * un jour peut-être :
       * pouvoir cliquer sur les graphiques pour voir la liste de résultats correspondant
           * par exemple pour les types d'inscription (à un service)
       * donuts sur les pays
           * pouvoir cliquer sur les résultats du camembert pour avoir une liste des chatons par pays
   * pages Uptimes (Federation, Organization, Services)
       * des améliorations à faire
           * TODO Cpm visibilité autres liens
           * TODO mrflos (pour l'été) : bidouiller la page statsuptime pour utiliser les filtres par état en js datatables
       * questions de statut manuel vs statut mesuré (page organization)
           * statut manuel seulement
           * statut mesuré seulement
           * les deux
           * un seul combiné des deux
           * discussion :
               * est-ce que la version manuelle est encore utile ? pertinence du mesuré
               * se poser la question de ce que cherche l'utilisateur
               * cas des statuts manuels « en travaux » ou  « fermé »
               * le statut manuel est plus important que le statut mesuré, respecté l'expression des admins
               * ne surtout pas afficher les deux
               * étudier la conjonction
           * TODO Cpm voir pour une version « combinée » avec bulle informative
   * Flo :
       * peut être avoir dans résumé des moyennes sans graphes, genre du texte « sur 2020 XXX visiteurs uniques, YYY ips différentes »
           * Cpm : une notion de « tendance » ?
           * Cpm : donner exemple ?
           * TODO Flo, à réfléchir l'enrichissement de texte des graphes toujours en TODO  > GT le 20 juillet > décalé
           * TODO Flo, à réfléchir à des cadres de tendances dans « Résumé » toujours en  > GT le 20 juillet > décalé
       * changer les intitulés « Web » et « Spécifique » par « Graphes de visites web » ou plus court « Graphes Web » et « Statistiques propres aux services » ou plus court « Stats des services » ?
           * Cpm : préciser l'intention
           * Flo : expliquer les items du menu type > GT le 20 juillet (annulé cause covid...)  > décalé
           * TODO Cpm : ajouter des bulles
           * TODO Flo : tester le menu métriques auprès de personnes
               * en cours toujours en TODO
           * TODO réfléchir
       * TODO Cpm afficher les champs nom et description des métrics dans les diagrammes
   * site statique vs site dynamique ?
       * différence de besoin entre le Chapril (métrique au jour) et le collectif CHATONS (mérique au mois)
       * supprimer les vues années, semaines et jours ?
       * supprimer les périodes 12 mois, 2020 et 2021 ?
       * site statique :
           * avantages : simplicité de déploiement (est-ce nécessaire ?)
           * inconvénients : prend de la place sur disque
       * pour site dynamique :
           * avantages : plus grande interactivité, plus de liberté fonctionnelles
           * inconvénients : nécessite l'installation d'un serveur d'application Java, du travail pour finaliser
       * avis :
           * Antoine : pas de problème au site dynamique
           * Flo : peut-être finir les fonctionnalités en cours avant de faire une version 2 dynamique avec des optimisations


### Revue générateur de fichiers métrics (statoolinfos)

   * ajout support Jitsi :
       * TODO Cpm en cours
   * ajout support Minetest
       * TODO Cpm 
   * ajout support PrivateBin
       * TODO Cpm en cours
   * metrics http
       * améliorer détection OS TODO Cpm
       * améliorer détection date TODO Cpm
   * question existentielle :
       * une visite http et un visiteur http, c'est au sens large, quelque soit le statut de la requête
       * corriger visit pour prendre pas seulement requête en succès
       * TODO Cpm FAIT, maintenant nb visitors < nb visits
       * sauf que non voir ontologie
       * TODO Cpm 


### Revue des catégories

   *  ([https://stats.chatons.org/category-autres.xhtml)](https://stats.chatons.org/category-autres.xhtml)) :
       * RAS


### Revue des fichiers properties de membres

   * passer en revue :
       *  [https://stats.chatons.org/chatons-crawl.xhtml](https://stats.chatons.org/chatons-crawl.xhtml)
           * plus tard
       * [https://stats.chatons.org/chatons-propertyalerts.xhtml](https://stats.chatons.org/chatons-propertyalerts.xhtml)
           * 316 rouges, 1226 jaunes, communiquer un jour prochain (au moment de la prochaine release - celle avec les metrics)
   * metrics spécifiques à chaque service à penser
       * besoin de repasser dessus pour le nommage avant de propager
       * besoin de coder leur affichage pour stats.chatons.org
       * besoin de paramétrer des moulinettes pour les récupérations automatisées de metrics
   * [https://stats.chatons.org/devloprog-cloud.xhtml](https://stats.chatons.org/devloprog-cloud.xhtml)
       * service.registration = None
   * [https://stats.chatons.org/framasoft-framadrive.xhtml](https://stats.chatons.org/framasoft-framadrive.xhtml)
       * service.registration.load


### Revue des tickets

   * [https://framagit.org/chatons/chatonsinfos/-/issues/1](https://framagit.org/chatons/chatonsinfos/-/issues/1)
       * Redesign des encarts au dessus des tableaux
       * prévu lorsqu'on aura toutes les informations affichées
       * statut : plus tard
   * [https://framagit.org/chatons/chatonsinfos/-/issues/2](https://framagit.org/chatons/chatonsinfos/-/issues/2)
       * Dans le tableau des services de la fiche organisation des chatons, supprimer la colonne "Organisation"
       * Cpm : ce tableau est une vue mutualisée entre plusieurs pages : organisation, services, catégorie,  logiciel ; l'information est effectivement redondante pour la page organisation, mais ça permet de conserver l'homogénéité de la vue.
       * Cpm : pour gagner de la place, possibilité de ne mettre que le logo de l'organisation et le nom en bulle
       * statut : réfléchir et sinon sera traité par la grande revue visuelle prévue un jour
   * [https://framagit.org/chatons/chatonsinfos/-/issues/4](https://framagit.org/chatons/chatonsinfos/-/issues/4)
       * Penser un nouveau fichier properties dédié aux offres non logicielles, celle d'hébergement
       * statut : priorité aux services utilisateurs donc pertinent mais plus tard


### Revue des merge requests

   *  [https://framagit.org/chatons/chatonsinfos/-/merge\_requests](https://framagit.org/chatons/chatonsinfos/-/merge\_requests)
       * [https://framagit.org/chatons/chatonsinfos/-/merge\_requests/41](https://framagit.org/chatons/chatonsinfos/-/merge\_requests/41)
           * Clawd.fr : Cpm FAIT


### Revue du forum

   * [https://forum.chatons.org/c/collectif/stats-chatons-org/83](https://forum.chatons.org/c/collectif/stats-chatons-org/83)
   * [https://forum.chatons.org/t/atelier-chatonsinfos-mercredi-19-janvier-2022-19h-20h/3139](https://forum.chatons.org/t/atelier-chatonsinfos-mercredi-19-janvier-2022-19h-20h/3139)
       * de bonnes intentions
   * [https://forum.chatons.org/t/chatonsinfos-version-0-4-en-avant-les-metriques/3082/6](https://forum.chatons.org/t/chatonsinfos-version-0-4-en-avant-les-metriques/3082/6)
       * question Laurent Sleto sur user vs account
       * TODO Cpm FAIT
   * [https://forum.chatons.org/t/chatonsinfos-version-0-4-en-avant-les-metriques/3082/8](https://forum.chatons.org/t/chatonsinfos-version-0-4-en-avant-les-metriques/3082/8)
       * idem
       * TODO Cpm FAIT


### Revue des disponibilités des services (uptimes)

   * [https://stats.chatons.org/chatons-uptimes.xhtml](https://stats.chatons.org/chatons-uptimes.xhtml)
   * Automario -> chat XMPP
       * Angie lui a envoyé un mp le 5/01 et il a dit qu'il avait fait le boulot (mais Angie le relance)
   * bug compteur bleu
       * TODO Cpm


### Revue de l'ontologie

   * rappels :
       * l'ordre des questions à se poser : préfixe, sous-préfixe
       * on rajout .count si le mot signifiant est le préfixe
       * métriques HTTP :
           * contexte :
               * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties)
               * [http://www.webalizer.org/webalizer\_help.html](http://www.webalizer.org/webalizer\_help.html)
                   * [https://web.archive.org/web/20210330151216/http://www.webalizer.org/webalizer\_help.html](https://web.archive.org/web/20210330151216/http://www.webalizer.org/webalizer\_help.html)


   * définition de visite :
       * actuellement, pour mesurer les visites, utilisation des logs http avec le statut SUCCESS
       * devons-nous considérer dans les visites les requêtes en erreur ? Y en a plein !
       * Webalizer (outil ancien mais qui est une référence) donne sa définition d'une visite :
           * [http://www.webalizer.org/webalizer\_help.html](http://www.webalizer.org/webalizer\_help.html)
               * [https://web.archive.org/web/20210330151216/http://www.webalizer.org/webalizer\_help.html](https://web.archive.org/web/20210330151216/http://www.webalizer.org/webalizer\_help.html)
           * « ** Visits** occur when some remote site makes a **[]request for a *page*[][] []**on your server […] Since only *pages* will trigger a visit, remotes sites that link to graphic and other non- page URLs will not be counted in the visit totals, reducing the number of *false* visits. »
       * « **Pages** are those URLs that would be considered the actual page being requested, and not all of the individual items that make it up (such as graphics and audio clips).  Some people call this metric *page views* or *page impressions*,  and defaults to any URL that has an extension of **.htm**, **.html** or **.cgi**. »
       * de nos jours, cette définition de page parait obsolète (nouvelles extensions comme xhtml, et beaucoup de pages sans extension) mrflos : +1
       * questions :
           * a-t-on utilité du comptage des visites de bots ?
               * mrflos : moi j'en aurai un usage détourné, comme un indicateur pour une attaque, mais peut etre d'autres outils seraient plus adaptés?
                   * ça fait rêver d'un metrics.http.attacks
       * donc quelle définition de visite adopter ? Proposition :
           * l'accès réussi à une ressource quelque soit son type sauf image, musique, vidéo…
           * metrics.http.visits : uniquement basé sur les logs SUCCESS et humaines
           * metrics.http.visits.humans -> remplacé par metrics.http.visits 
           * metrics.http.visits.bots -> supprimé car aucun intérêt
               * mrflos: si je suis le seul a voir un intéret a compter les bots, je consens à cette proposition, mais si cela ne coute pas grand chose a le laisser, ca me va de le garder
                   * compter les bots ou les visites de bots ? Les bots sont compter via metric.http.requesters.bots
                   * coût plutôt faible
                   * la visite d'un bot n'a pas la même définition que la visite d'une personne, du coup peut-on encore parler de visite pour un bot ?
                   * comment nommer le métrique ?
   * définition de visitor :
       * si la notion de visite se limite aux logs SUCCESS et humain alors celle de visiteur aussi ?
           * mrflos: pour moi c'est les visites success ne concernant pas les fichiers de type assets (css, js, images jpe?g, gif, png, svg,.. mais peut etre cette liste est difficile à maintenir..)
               * plutôt que de dire ce qui est une page, dire ce qui n'en est pas une, bien vu !
               * cette liste n'a pas besoin d'être exhaustive, on peut déjà gérer une bonne liste d'extensions, ça fait converger vers le but
                   * TODO Cpm ajouter critère dans le code FAIT
       * inventer un nouveau métric pour différencier d'avec visitor ?
       * metrics.http.requesters ?
           *  metrics.http.internaut ? (pour internaute)
               * un bot est-il un internaute ?
       * et dans ce cas on s'en fiche d'avoir la notion des visits de requesters ?
       * propositions :
           * metrics.http.requesters 
           * metrics.http.requesters.ipv4
           * metrics.http.requesters.ipv6
           * metrics.http.requesters.humans
           * metrics.http.requesters.humans.ipv4
           * metrics.http.requesters.humans.ipv6
           * metrics.http.requesters.bots
           * metrics.http.requesters.bots.ipv4
           * metrics.http.requesters.bots.ipv6
           * metrics.http.visitors : uniquement basé sur les logs SUCCESS + humaines + GET + pas assets non pasges
           * metrics.http.visitors.humans -> remplacé par metrics.http.visitors
           * metrics.http.visitors.bots -> supprimé
           * metrics.http.visitors.ipv4
           * metrics.http.visitors.ipv6
   * itération n°2, étape 1 :
       * on a des stats sur les hits (requêtes web), il nous faut des stats sur les auteurs de ces hits. Actuellement, c’est visitors qui les portent sauf que c’est une erreur car visitors porte les stats sur les visites qui répondent à des critères plus restrictifs. Du coup, il nous faut un métric dédié à décliner en .ipv4, .ipv6, .humans et .bots. Comment préfixer ce nouveaux métric ?
           *  ~~http.hiters~~ vs ~~http.hitters~~ vs http.requesters vs ~~http.internauts~~ vs ~~http.webonautes~~
               * Mrflos : j’aime pas les hiters/hitters , mais les 2 autres me vont, ma préférence allait pour internauts qui rappelle que c’est un accès web, mais est-ce assez générique pour accueillir des robots en déclinaison? requesters est ok pour moi si internaut n’est pas apprécié
           * choix : http.requesters
   * itération n°2, étape 2 :
       * ensuite, on voudrait des stats sur les visites et sur les visiteurs. Historiquement (Awstats), la notion de visite est associée à l’activité des utilisateurs humains (en opposition aux bots). Du coup, la définition d’une visite est très restrictive : userAgent non bot + GET + status SUCCESS + pas image/vidéo/son. Par extension, les visiteurs sont les auteurs des visites et donc ont également une définition plus restrictive par rapport à http.requesters.humans. Proposition logique : http.visits, http.visits.ipv4, http.visits.ipv6, http.visitors, http.visitors.ipv4, http.visitors.ipv6.
           * Mrflos : ok pour moi, mais spoiler, ya un twist prochainement 
   * itération n°2, étape 3 :
       * enfin, on voudrait des stats sur les visites des bots. Mais ces visites ne répondent pas aux mêmes critères que pour les visites humaines. En fait, il n’y a aucun critère de restriction, toute requête d’un bot est une visite. Du coup :
           * http.visitors.bots est égal à http.requesters.bots et donc ça fait doublon ;
           * est-ce pertinent de mettre dans http.visits des sous-métrics avec des définitions différentes (http.visits.bots et http.visits.humans) ?
           * si un bot se fait passer pour un humain en tentant des requêtes toutes en 404 alors la visite ne sera comptabilisée ni dans http.visitors.bots ni dans http.visitors.humans.
       * Possibilité de créer un métrics dédié au comptage des *visites* de bots. Ranger ce métrics ailleurs que dans ‹ http.visits › éviterait la confusion.
           * un synonyme de « visits » ? series, ~~campaigns~~, trips, tours, rides, ~~raids~~, ~~vacations~~, ~~steeps~~, ~~scraps~~ ?
           * http.requesters.bots.rides vs http.rides vs http.bots.rides
           * http.requesters.bots.series
           * http.requesters.bots.rides
           * http.requesters.bots.walks +
           * inteval de temps minimal entre deux visites de bots ?
       * Mrflos : sinon, pour « simplifier » (voire retourner la table)
           * les hits : 
               * http.hits.humans, http.hits.humans.ipv4, http.hits.humans.ipv6
               * http.hits.bots, http.hits.bots.ipv4, http.hits.bots.ipv6
           * les visites
               * http.visits.humans, http.visits.humans.ipv4, http.visits.humans.ipv6
               * http.visits.bots, http.visits.bots.ipv4, http.visits.bots.ipv6
       * ou
           * les humains
               * http.humans.hits, http.humans.hits.ipv4, http.humans.hits.ipv6
               * http.humans.visits, http.humans.visits.ipv4, http.humans.visits.ipv6
           * les machines
               * http.bots.hits, http.bots.hits.ipv4, http.bots.hits.ipv6
               * http.bots.visits, http.bots.visits.ipv4, http.bots.visits.ipv6
               * http.bots.attacks (pour le 4.)
   * itération n°2, étape 4 :
       * on pourrait vouloir avoir des stats sur les attaques (par exemple, plein de requêtes Wordpress sur un service Framadate). Quel métrics utiliser ? http.requesters.bot.attacks ?
           * Mrflos : ok si 3. pas retenue


   * nouveau/modification du modèle :
       * metrics.filesharing.files.count -> metrics.filesharing.files ?


   * fiche properties hébergement
       * réclamée donc pourquoi ne pas l'envisager
       * après plusieurs tentatives exploratoires, décision de tenter une fichier spécifique
       * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/hosting.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/hosting.properties)
       * cas des vps :
           * vps.os
           * vps.hypervisor
           * vps.
           * la question se pose également pour la section host normale
       * projection d'intégration dans stats.chatons.org
           * besoin d'un bouton « Services dédiés »
           * certainement renommer « Services » en « Services mutualisés »
           * page d'index : 
               * ajouter un nombre spécifique pour les services dédiés
               * ajouter un indicateur pour différencier
               * dans le tableau : un colonne pour les services dédiés / une pour les services mutualisés
           * page d'un membre :
               * ajouter un nombre spécifique pour les services dédiés
               * ajouter un indicateur pour différencier
           * nommage : service mutualisé vs service dédié
           * faire des essais
               * se sont des travaux préparatoires, priorité à l'objectif en cours
               * TODO Angie : créer une fiche "fausse" fiche hosting.properties pour Framasoft afin de tester
                   * [https://framagit.org/framasoft/chatons/-/blob/main/Fichiers\_properties/hosting-nextcloud.properties](https://framagit.org/framasoft/chatons/-/blob/main/Fichiers\_properties/hosting-nextcloud.properties)




## 57e réunion du groupe de travail

**jeudi 03 février 2022 à 11h15**

Personnes présentes : Angie / Cpm / Jeey


### Divers

   * divers précédents :
       * création d'un schéma explicitant les subs
           * TODO Flo+Jeey
           * [https://excalidraw.com/#json=CKiP-SnK6uSXYvNIZHPP\_,CaVeIe\_gJNVMVDSbWdiqdA](https://excalidraw.com/#json=CKiP-SnK6uSXYvNIZHPP\_,CaVeIe\_gJNVMVDSbWdiqdA)
       * demande d'amélioration de la doc# sur subs.foo (Zatalyz)
           * Voir conversation zatalys sur le forum et lui répondre
           * TODO Flo+Jeey
       * mrflos/Jeey faire un schéma pour le README.md qui montre l'arborescence entre les fichiers properties
           * répondre sur le forum
           * en cours (mais on va privilégier le papier, le paquet yunohost d'excalidraw n'étant pas mûr ;) )


### Revue de [https://stats.chatons.org/](https://stats.chatons.org/) 

   * vue liste d'organisation + liste vue liste de services :
       * ajouter bulle et astérisk pour indiquer le mois des entêtes « mensuelles »
           * TODO Cpm FAIT
       * ajout emoji pour remplacer zéro
           * TODO Cpm FAIT
   * page CHATONS (accueil) : 
       * vérification du nombre de membres      /!\ <----------------------------------
           * 99  ACTIVE 
               * TODO Angie ajouter fichier properties Altertek
           * 6 IDLE
               * Le Samarien
               * gozmail
               * Boblecodeur
               * Octodome
               * Duchesse
               * Devosi (pas de fiche CHATONS)
           * 1 AWAY
           * donc ne pas afficher les IDLE sur la page d'accueil ? yep
               * TODO Cpm
       * plus tard éventuellement, ajout d'un fonction pour voir les autres aussi "le cimetière des chatons" 😆
           * sera facilement faisable avec la version dynamique
           * TODO Cpm
       * ajout d'émoticon suite à remarque que les zéro n'étaient pas cool
           * maintenir ~~ou mettre blanc~~ ?
               * OUI \o/
   * page générique d'un chaton : 
       * penser à augmenter le code html avec les informations de properties pour faciliter le futur réagencement  UI/UX
           * TODO Cpm
   * page « Statistiques » (fédération) :
       * ajouter un donuts sur les services de paiement
           * TODO Cpm 
   * un jour peut-être :
       * pouvoir cliquer sur les graphiques pour voir la liste de résultats correspondant
           * par exemple pour les types d'inscription (à un service)
       * donuts sur les pays
           * pouvoir cliquer sur les résultats du camembert pour avoir une liste des chatons par pays
   * pages Uptimes (Federation, Organization, Services)
       * des améliorations à faire
           * TODO Cpm visibilité autres liens
           * TODO mrflos (pour l'été) : bidouiller la page statsuptime pour utiliser les filtres par état en js datatables
       * questions de statut manuel vs statut mesuré (page organization)
           * statut manuel seulement
           * statut mesuré seulement
           * les deux
           * un seul combiné des deux
           * discussion :
               * est-ce que la version manuelle est encore utile ? pertinence du mesuré
               * se poser la question de ce que cherche l'utilisateur
               * cas des statuts manuels « en travaux » ou  « fermé »
               * le statut manuel est plus important que le statut mesuré, respecté l'expression des admins
               * ne surtout pas afficher les deux
               * étudier la conjonction
           * TODO Cpm voir pour une version « combinée » avec bulle informative
   * Flo :
       * peut être avoir dans résumé des moyennes sans graphes, genre du texte « sur 2020 XXX visiteurs uniques, YYY ips différentes »
           * Cpm : une notion de « tendance » ?
           * Cpm : donner exemple ?
           * TODO Flo, à réfléchir l'enrichissement de texte des graphes toujours en TODO  > GT le 20 juillet > décalé
           * TODO Flo, à réfléchir à des cadres de tendances dans « Résumé » toujours en  > GT le 20 juillet > décalé
       * changer les intitulés « Web » et « Spécifique » par « Graphes de visites web » ou plus court « Graphes Web » et « Statistiques propres aux services » ou plus court « Stats des services » ?
           * Cpm : préciser l'intention
           * Flo : expliquer les items du menu type > GT le 20 juillet (annulé cause covid...)  > décalé
           * TODO Cpm : ajouter des bulles
           * TODO Flo : tester le menu métriques auprès de personnes
               * en cours toujours en TODO
           * TODO réfléchir
       * TODO Cpm afficher les champs nom et description des métrics dans les diagrammes
   * site statique vs site dynamique ?
       * différence de besoin entre le Chapril (métrique au jour) et le collectif CHATONS (mérique au mois)
       * supprimer les vues années, semaines et jours ?
       * supprimer les périodes 12 mois, 2020 et 2021 ?
       * site statique :
           * avantages : simplicité de déploiement (est-ce nécessaire ?)
           * inconvénients : prend de la place sur disque
       * pour site dynamique :
           * avantages : plus grande interactivité, plus de liberté fonctionnelles
           * inconvénients : nécessite l'installation d'un serveur d'application Java, du travail pour finaliser
       * avis :
           * Antoine : pas de problème au site dynamique
           * Flo : peut-être finir les fonctionnalités en cours avant de faire une version 2 dynamique avec des optimisations


### Revue générateur de fichiers métrics (statoolinfos)

   * ajout support Jitsi :
       * TODO Cpm en cours
   * ajout support Minetest
       * TODO Cpm 
   * ajout support PrivateBin
       * TODO Cpm en cours
   * metrics http
       * améliorer détection OS TODO Cpm
       * améliorer détection date TODO Cpm
   * question existentielle :
       * ajout métrics requesters
       * une visite http et un visiteur http, c'est au sens large, quelque soit le statut de la requête
       * corriger visit pour prendre pas seulement requête en succès
       * TODO Cpm FAIT, maintenant nb visitors < nb visits


### Revue des catégories

   *  ([https://stats.chatons.org/category-autres.xhtml)](https://stats.chatons.org/category-autres.xhtml)) :
       * RAS


### Revue des fichiers properties de membres

   * passer en revue :
       *  [https://stats.chatons.org/chatons-crawl.xhtml](https://stats.chatons.org/chatons-crawl.xhtml)
           * plus tard
       * [https://stats.chatons.org/chatons-propertyalerts.xhtml](https://stats.chatons.org/chatons-propertyalerts.xhtml)
           * 292 ~~316~~ rouges, 1220 ~~1226~~ jaunes, communiquer un jour prochain (au moment de la prochaine release - celle avec les metrics)
   * metrics spécifiques à chaque service à penser
       * besoin de repasser dessus pour le nommage avant de propager
       * besoin de coder leur affichage pour stats.chatons.org
       * besoin de paramétrer des moulinettes pour les récupérations automatisées de metrics
   * [https://stats.chatons.org/devloprog-cloud.xhtml](https://stats.chatons.org/devloprog-cloud.xhtml)
       * service.registration = None
       * TODO Contacter
   * [https://stats.chatons.org/framasoft-framadrive.xhtml](https://stats.chatons.org/framasoft-framadrive.xhtml)
       * service.registration.load
       * TODO Angie
   * [https://stats.chatons.org/sansnuagearn-mail-propertycheck.xhtml](https://stats.chatons.org/sansnuagearn-mail-propertycheck.xhtml)
       * service.enddate = 27/11/2018 
       * service.status.level = OK 
       * contacter par courriel, fait le 22/01/2022
           * TODO Cpm FAIT
       * TODO attendre la réponse
   * [https://stats.chatons.org/.well-known/chatonsinfos/organizations.default/drycat.properties](https://stats.chatons.org/.well-known/chatonsinfos/organizations.default/drycat.properties)
       * organization.status.level=ACTIVE
       * mettre à AWAY
           * TODO Cpm FAIT
   *  [https://stats.chatons.org/automario-2ec5d5fd.xhtml](https://stats.chatons.org/automario-2ec5d5fd.xhtml)
       * déclaration d'un fichier métrics dans un fichier organization
       * TODO Contacter
           * Cpm FAIT via le forum
       * TODO attendre prise en compte
           * FAIT
   * absence de organization.status.level :
       * [https://stats.chatons.org/evolix-propertycheck.xhtml](https://stats.chatons.org/evolix-propertycheck.xhtml)
       * [https://stats.chatons.org/flap-propertycheck.xhtml](https://stats.chatons.org/flap-propertycheck.xhtml)
       * [https://stats.chatons.org/garbaye.xhtml](https://stats.chatons.org/garbaye.xhtml)
       * [https://stats.chatons.org/isidorus-propertycheck.xhtml](https://stats.chatons.org/isidorus-propertycheck.xhtml)
       * [https://stats.chatons.org/kaz-propertycheck.xhtml](https://stats.chatons.org/kaz-propertycheck.xhtml)
       * [https://stats.chatons.org/liberta-propertycheck.xhtml](https://stats.chatons.org/liberta-propertycheck.xhtml)
       * [https://stats.chatons.org/libreon-propertycheck.xhtml](https://stats.chatons.org/libreon-propertycheck.xhtml)
       * un reste de la 12e portée :
           * [https://forum.chatons.org/t/resultat-des-candidatures-pour-la-12eme-portee/2545](https://forum.chatons.org/t/resultat-des-candidatures-pour-la-12eme-portee/2545)
       * champs :
           * organization.status.level =
           * organization.memberof.chatons.startdate =
           *  organization.memberof.chatons.status.level =
       * TODO Cpm FAIT
       * question : si modifs sur le sftp alors pas écrasés dans le futur ?
   * stats membres 2022 :
       * [https://stats.chatons.org/chatons-stats.xhtml](https://stats.chatons.org/chatons-stats.xhtml)
       * proposition de mettre des valeurs pour 2022
       * sur le site sftp directement ?
       * TODO Angie


### Revue des tickets

   * [https://framagit.org/chatons/chatonsinfos/-/issues/1](https://framagit.org/chatons/chatonsinfos/-/issues/1)
       * Redesign des encarts au dessus des tableaux
       * prévu lorsqu'on aura toutes les informations affichées
       * statut : plus tard
   * [https://framagit.org/chatons/chatonsinfos/-/issues/2](https://framagit.org/chatons/chatonsinfos/-/issues/2)
       * Dans le tableau des services de la fiche organisation des chatons, supprimer la colonne "Organisation"
       * Cpm : ce tableau est une vue mutualisée entre plusieurs pages : organisation, services, catégorie,  logiciel ; l'information est effectivement redondante pour la page organisation, mais ça permet de conserver l'homogénéité de la vue.
       * Cpm : pour gagner de la place, possibilité de ne mettre que le logo de l'organisation et le nom en bulle
       * statut : réfléchir et sinon sera traité par la grande revue visuelle prévue un jour
   * [https://framagit.org/chatons/chatonsinfos/-/issues/4](https://framagit.org/chatons/chatonsinfos/-/issues/4)
       * Penser un nouveau fichier properties dédié aux offres non logicielles, celle d'hébergement
       * statut : priorité aux services utilisateurs donc pertinent mais plus tard


### Revue des merge requests

   * [https://framagit.org/chatons/chatonsinfos/-/merge\_requests/42](https://framagit.org/chatons/chatonsinfos/-/merge\_requests/42)
       * ajout URL fichier properties organisation Le Pic
       * TODO Cpm FAIT


### Revue du forum

   * [https://forum.chatons.org/c/collectif/stats-chatons-org/83](https://forum.chatons.org/c/collectif/stats-chatons-org/83)
   * RAS


### Revue des disponibilités des services (uptimes)

   * [https://stats.chatons.org/chatons-uptimes.xhtml](https://stats.chatons.org/chatons-uptimes.xhtml)
   * Automario -> chat XMPP
       * Angie lui a envoyé un mp le 5/01 et il a dit qu'il avait fait le boulot (mais Angie le relance encore)
       * TODO attendre réponse
   * bug compteur bleu
       * TODO Cpm FAIT


### Revue de l'ontologie

   * rappels :
       * l'ordre des questions à se poser : préfixe, sous-préfixe
       * on rajout .count si le mot signifiant est le préfixe
       * métriques HTTP :
           * contexte :
               * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties)
               * [http://www.webalizer.org/webalizer\_help.html](http://www.webalizer.org/webalizer\_help.html)
                   * [https://web.archive.org/web/20210330151216/http://www.webalizer.org/webalizer\_help.html](https://web.archive.org/web/20210330151216/http://www.webalizer.org/webalizer\_help.html)


   * nouvelle définition de visite :
       * propagation http.requesters + http.visits + http.visitors :
           * code du générateur de fichier properties métrics
           * code de génération du site stats.chatons.org
           * model metrics.properties
           * CHANGELOG
           * release
           * deploiement sur stats.chatons.org
       * TODO Cpm FAIT


   * nouveau/modification du modèle :
       * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties#L825](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties#L825)
       * metrics.filesharing.files.count -> metrics.filesharing.files ?
       * maj CHANGELOG
       * TODO Angie FAIT


   * nouveau metric http :
       * metric.http.methods.XXX
       * information technique d'une requête, simple à faire
       * TODO :
           * ajout dans fichier properties modèle
           * codage générateur de fichier properties de métric
           * CHANGELOG
           * Cpm : FAIT


   * nouveau metric http : 
       * metrics.service.files : nombre de fichiers
       * pas encore trouvé l'utilité du point de vue du collectif


   * refonte de organization.memberof
       * actuellement :
           * organization.status.level =
           * organization.status.description = 
           * organization.startdate = 
           * organization.enddate =
           * organization.memberof.XXX.startdate = 
           * organization.memberof.XXX.enddate = 
           * organization.memberof.XXX.status.level = 
           * organization.memberof.XXX.status.description =
       * les inconvénients :
           * code compliqué
           * simplification possible du nommage ?
           * avoir une fiche organization pour chaque fédération ?
               * remet en cause le système existant
               * et en plus ça obligerait à des copies de chaque fiche organization


   * fiche properties hébergement
       * réclamée donc pourquoi ne pas l'envisager
       * après plusieurs tentatives exploratoires, décision de tenter une fichier spécifique
       * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/hosting.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/hosting.properties)
       * ~~cas des vps :~~
           * ~~vps.os~~
           * ~~vps.hypervisor~~
               * ~~vps.~~
           * la question se pose également pour la section host normale
               * d'ailleurs le cas vps.* est traitable dans host.*
               * recenser les systèmes d'exploitation des vm et des hosts ?
                   * host.os
               * recenser les systèmes d'hypervision des vm et des hosts ?
                   * host.hypervisor
               * service.os // hosting.os : compliqué à exprimer / ne correspond pas vraiment
           * on collecte déjà beaucoup d'informations,  est-ce bien nécessaire ?
               * permettrait de faire se questionner sur le logiciel de supervision utilisé
       * projection d'intégration dans stats.chatons.org
           * besoin d'un bouton « Services dédiés » 
           * certainement renommer « Services » en « Services mutualisés » +1
           * page d'index : 
               * ajouter un nombre spécifique pour les services dédiés
               * ajouter un indicateur pour différencier
               * dans le tableau : un colonne pour les services dédiés / une pour les services mutualisés
           * page d'un membre :
               * ajouter un nombre spécifique pour les services dédiés
               * ajouter un indicateur pour différencier
           * nommage : service mutualisé vs service dédié
           * faire des essais
               * TODO Angie FAIT : créer une fiche "fausse" fiche hosting.properties pour Framasoft afin de tester
                   * [https://framagit.org/framasoft/chatons/-/blob/main/Fichiers\_properties/hosting-nextcloud.properties](https://framagit.org/framasoft/chatons/-/blob/main/Fichiers\_properties/hosting-nextcloud.properties)
               * TODO Angie : demander à ljf de compléter une fiche hosting.properties pour l'une des offres d'hébergement qu'il propose


## 58e réunion du groupe de travail

**jeudi 03 mars 2022 à 11h15**

Personnes présentes : MrFlos, Angie, Cpm

### Divers

   * divers précédents :
       * création d'un schéma explicitant les subs
           * TODO Flo+Jeey
           * [https://excalidraw.com/#json=CKiP-SnK6uSXYvNIZHPP\_,CaVeIe\_gJNVMVDSbWdiqdA](https://excalidraw.com/#json=CKiP-SnK6uSXYvNIZHPP\_,CaVeIe\_gJNVMVDSbWdiqdA)
       * demande d'amélioration de la doc# sur subs.foo (Zatalyz)
           * Voir conversation zatalys sur le forum et lui répondre
           * TODO Flo+Jeey
       * mrflos/Jeey faire un schéma pour le README.md qui montre l'arborescence entre les fichiers properties
           * répondre sur le forum
           * en cours (mais on va privilégier le papier, le paquet yunohost d'excalidraw n'étant pas mûr ;) )
   * rapport activité GT stats.chatons.org
       * [https://mypads.framapad.org/p/rapport-d-activite-2021-9nbi974t](https://mypads.framapad.org/p/rapport-d-activite-2021-9nbi974t)
       * TODO Cpm FAIT
       * TODO tout le monde : relire/compléter :D FAIT


### Revue de [https://stats.chatons.org/](https://stats.chatons.org/) 

   * page CHATONS (accueil) : 
       * vérification du nombre de membres      /!\ <----------------------------------
           * 98  ACTIVE 
               * TODO Angie ajouter fichier properties Altertek FAIT
               * => KO : 98
           * 6 IDLE
               * Le Samarien, gozmail, Boblecodeur, Octodome, Duchesse, Devosi (pas de fiche CHATONS) => OK
           * 1 AWAY => OK
           * donc ne pas afficher les IDLE sur la page d'accueil ? yep
               * TODO Cpm FAIT
           * ajout liste des membres en sommeil
               * TODO Cpm FAIT
           * reformulation label compteur au dessus du tableau :
               * avant :
                   * Nombre de membres : 98
                   * Nombre de services : 270
               * proposition n°1 :
                   * Nombre de membres actifs : 98
                   * Nombre de services actifs : 270
               * proposition n°2 :
                   * Membres actifs : 98
                   * Services actifs : 270
               * proposition n°3 :
                   * 98 membres actifs
                   * 270  services actifs
               * choix :  n°3
               * TODO Cpm 
       * plus tard éventuellement, ajout d'un fonction pour voir les autres aussi "le cimetière des chatons" 😆
           * ajout liste des anciens membres
           * TODO Cpm FAIT
   * page générique d'un chaton : 
       * penser à augmenter le code html avec les informations de properties pour faciliter le futur réagencement  UI/UX
           * TODO Cpm
   * page « Statistiques » (fédération) :
       * ajouter un donuts sur les services de paiement
           * TODO Cpm 
   * un jour peut-être :
       * pouvoir cliquer sur les graphiques pour voir la liste de résultats correspondant
           * par exemple pour les types d'inscription (à un service)
       * donuts sur les pays
           * pouvoir cliquer sur les résultats du camembert pour avoir une liste des chatons par pays
   * pages Uptimes (Federation, Organization, Services)
       * des améliorations à faire
           * TODO Cpm visibilité autres liens
           * TODO mrflos (pour l'été) : bidouiller la page statsuptime pour utiliser les filtres par état en js datatables
       * questions de statut manuel vs statut mesuré (page organization)
           * statut manuel seulement
           * statut mesuré seulement
           * les deux
           * un seul combiné des deux
           * discussion :
               * est-ce que la version manuelle est encore utile ? pertinence du mesuré
               * se poser la question de ce que cherche l'utilisateur
               * cas des statuts manuels « en travaux » ou  « fermé »
               * le statut manuel est plus important que le statut mesuré, respecté l'expression des admins
               * ne surtout pas afficher les deux
               * étudier la conjonction
           * TODO Cpm voir pour une version « combinée » avec bulle informative
   * Flo :
       * peut être avoir dans résumé des moyennes sans graphes, genre du texte « sur 2020 XXX visiteurs uniques, YYY ips différentes »
           * Cpm : une notion de « tendance » ?
           * Cpm : donner exemple ?
           * TODO Flo, à réfléchir l'enrichissement de texte des graphes toujours en TODO  > GT le 20 juillet > décalé
           * TODO Flo, à réfléchir à des cadres de tendances dans « Résumé » toujours en  > GT le 20 juillet > décalé
       * changer les intitulés « Web » et « Spécifique » par « Graphes de visites web » ou plus court « Graphes Web » et « Statistiques propres aux services » ou plus court « Stats des services » ?
           * Cpm : préciser l'intention
           * Flo : expliquer les items du menu type > GT le 20 juillet (annulé cause covid...)  > décalé
           * TODO Cpm : ajouter des bulles
           * TODO Flo : tester le menu métriques auprès de personnes
               * en cours toujours en TODO
           * TODO réfléchir
       * TODO Cpm afficher les champs nom et description des métrics dans les diagrammes
   * site statique vs site dynamique ?
       * différence de besoin entre Libre-service.eu (métrique au jour) et le collectif CHATONS (mérique au mois)
       * supprimer les vues années, semaines et jours ?
       * supprimer les périodes 12 mois, 2020 et 2021 ?
       * site statique :
           * avantages : simplicité de déploiement (est-ce nécessaire ?)
           * inconvénients : prend de la place sur disque
       * pour site dynamique :
           * avantages : plus grande interactivité, plus de liberté fonctionnelles
           * inconvénients : nécessite l'installation d'un serveur d'application Java, du travail pour finaliser
       * avis :
           * Antoine : pas de problème au site dynamique
           * Flo : peut-être finir les fonctionnalités en cours avant de faire une version 2 dynamique avec des optimisations
   * Test disponibilité en ipv6 :
       * [https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232](https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232)
       * est-ce une information déclarative ou à calculer ?
           * mieux si c'est calculé car plus à jour et un truc en moins à faire pour les admins
       * sujet non urgent, à approfondir
       * continuer à évaluer les besoins
       * page ipv6 pour avoir un aperçu du point de vue du collectif
   * Vérifier la présence de trackers basic. 
       * [https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232](https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232)
       * oui, très pertinent
       * valorisation : à envisager page dédié


### Revue générateur de fichiers métrics (statoolinfos)

   * ajout support Jitsi :
       * TODO Cpm en cours
   * ajout support Minetest
       * TODO Cpm 
   * ajout support PrivateBin
       * TODO Cpm en cours
   * ajout support Gitea
       * TODO Cpm en cours
   * ajout support Etherpad
   * ajout support Nextcloud
   * ajout support XMPP
   * metrics http
       * améliorer détection OS TODO Cpm
       * améliorer détection date TODO Cpm
   * pour info: mrflos travaille sur scrumblr et pourra faire la page stat
       * \o/


### Revue des catégories

   *  ([https://stats.chatons.org/category-autres.xhtml)](https://stats.chatons.org/category-autres.xhtml)) :
   * catégorie Agenda :
       * actuellement : Nextcloud Calendar, Radicale, SOgo, Tracim
       * rajouter Nextcloud ?
       * TODO Angie : check sur les modules par défaut sur NC et ajouter en fonction "Nextcloud"
   * rajouter Baïkal ?
       * site web : [https://sabre.io/baikal/](https://sabre.io/baikal/)
       * sources : [https://github.com/sabre-io/Baikal](https://github.com/sabre-io/Baikal)
       * licence : GNU GPLv3
       * source licence : [https://github.com/sabre-io/Baikal/blob/master/LICENSE](https://github.com/sabre-io/Baikal/blob/master/LICENSE)
       * catégorie : agenda ?
       * TODO Angie
   * PeerJS Server
       * "A server side element to broker connections between PeerJS clients."
       * service aux utilisateurs ?
       * TODO Angie : vérifier et ajouter / demander la suppression de la fiche


### Revue des fichiers properties de membres

   * passer en revue :
       *  [https://stats.chatons.org/chatons-crawl.xhtml](https://stats.chatons.org/chatons-crawl.xhtml)
           * plus tard
       * [https://stats.chatons.org/chatons-propertyalerts.xhtml](https://stats.chatons.org/chatons-propertyalerts.xhtml)
           * 289 ~~292~~ ~~316~~ rouges, 1217 ~~1220~~ ~~1226~~ jaunes, communiquer un jour prochain (au moment de la prochaine release - celle avec les metrics)
   * metrics spécifiques à chaque service à penser
       * besoin de repasser dessus pour le nommage avant de propager
       * besoin de coder leur affichage pour stats.chatons.org
       * besoin de paramétrer des moulinettes pour les récupérations automatisées de metrics
   * [https://stats.chatons.org/devloprog-cloud.xhtml](https://stats.chatons.org/devloprog-cloud.xhtml)
       * service.registration = None
       * TODO Angie Contacter pour lui indiquer que le champs est mal rempli ("sans inscription" pour un NC)
   * [https://stats.chatons.org/framasoft-framadrive.xhtml](https://stats.chatons.org/framasoft-framadrive.xhtml)
       * service.registration.load
       * FAIT Angie
   * [https://stats.chatons.org/sansnuagearn-mail-propertycheck.xhtml](https://stats.chatons.org/sansnuagearn-mail-propertycheck.xhtml)
       * service.enddate = 27/11/2018 
       * service.status.level = OK 
       * contacté par courriel le 22/01/2022
       * TODO attendre la réponse
       * FAIT
   * stats membres 2022 :
       * [https://stats.chatons.org/chatons-stats.xhtml](https://stats.chatons.org/chatons-stats.xhtml)
       * proposition de mettre des valeurs pour 2022
       * sur le site sftp directement ?
       * TODO Angie FAIT
   * Underworld :
       * [https://stats.chatons.org/underworld-crawl.xhtml](https://stats.chatons.org/underworld-crawl.xhtml)
       * contacté pour prévenir
       * TODO attendre réponse
       * corrigé, FAIT
   * Duchesse :
       * [https://stats.chatons.org/duchesse-propertycheck.xhtml](https://stats.chatons.org/duchesse-propertycheck.xhtml)
       * date d'entrée dans le collectif ?
       * TODO Angie


### Revue des tickets

   * [https://framagit.org/chatons/chatonsinfos/-/issues/1](https://framagit.org/chatons/chatonsinfos/-/issues/1)
       * Redesign des encarts au dessus des tableaux
       * prévu lorsqu'on aura toutes les informations affichées
       * statut : plus tard
   * [https://framagit.org/chatons/chatonsinfos/-/issues/2](https://framagit.org/chatons/chatonsinfos/-/issues/2)
       * Dans le tableau des services de la fiche organisation des chatons, supprimer la colonne "Organisation"
       * Cpm : ce tableau est une vue mutualisée entre plusieurs pages : organisation, services, catégorie,  logiciel ; l'information est effectivement redondante pour la page organisation, mais ça permet de conserver l'homogénéité de la vue.
       * Cpm : pour gagner de la place, possibilité de ne mettre que le logo de l'organisation et le nom en bulle
       * statut : réfléchir et sinon sera traité par la grande revue visuelle prévue un jour
   * [https://framagit.org/chatons/chatonsinfos/-/issues/4](https://framagit.org/chatons/chatonsinfos/-/issues/4)
       * Penser un nouveau fichier properties dédié aux offres non logicielles, celle d'hébergement
       * statut : priorité aux services utilisateurs donc pertinent mais plus tard


### Revue des merge requests

   * [https://framagit.org/framasoft/chatons/-/merge\_requests](https://framagit.org/framasoft/chatons/-/merge\_requests)
   * [https://framagit.org/chatons/chatonsinfos/-/merge\_requests/43](https://framagit.org/chatons/chatonsinfos/-/merge\_requests/43)
       * Ajout URL pour Garbaye. 
       * TODO Cpm FAIT
   * [https://framagit.org/chatons/chatonsinfos/-/merge\_requests/44](https://framagit.org/chatons/chatonsinfos/-/merge\_requests/44)
       * ajout URL pour Altertek
       * TODO Cpm FAIT


### Revue du forum

   * [https://forum.chatons.org/c/collectif/stats-chatons-org/83](https://forum.chatons.org/c/collectif/stats-chatons-org/83)
   * Suggestions pour la version 0.6+
       * [https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232](https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232)
       * TODO Mrflos Cpm FAIT


### Revue des disponibilités des services (uptimes)

   * [https://stats.chatons.org/chatons-uptimes.xhtml](https://stats.chatons.org/chatons-uptimes.xhtml)
   * Automario -> chat XMPP
       * Angie lui a envoyé un mp le 5/01 et il a dit qu'il avait fait le boulot (mais Angie le relance encore)
       * TODO attendre réponse
       * corrigé, FAIT
   * Limesurvey  Nomagic
       * rouge, à surveiller pour la prochaine fois
   * Wekan      Picasoft
       * rouge, à surveiller pour la prochaine fois


### Revue de l'ontologie

   * rappels :
       * l'ordre des questions à se poser : préfixe, sous-préfixe
       * on rajout .count si le mot signifiant est le préfixe
       * métriques HTTP :
           * contexte :
               * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties)
               * [http://www.webalizer.org/webalizer\_help.html](http://www.webalizer.org/webalizer\_help.html)
                   * [https://web.archive.org/web/20210330151216/http://www.webalizer.org/webalizer\_help.html](https://web.archive.org/web/20210330151216/http://www.webalizer.org/webalizer\_help.html)


   * metrics.forge.projects vs metrics.forge.repositories
       * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties#L404](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties#L404)
       * 1 projet = 1 repositories => doublon ? lequel conserver ?
       * pas doublon (voir Gitlab CE)


   * metrics.barcodes.count -> metrics.barcodes.created/generated
       * explicitation
       * évite le terme « count »
       * avis : oui
       * TODO metrics.properties  FAIT par angie
       * TODO générateur métrics Cpm


   * metrics.service.files.bytes -> metrics.service.datafiles.bytes.
       * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties#L240](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties#L240)
       * écho avec metrics.service.database.bytes
       * permet d'éviter de se demander si ce sont les fichiers de données ou si sont comptés aussi les fichiers de code
       * pas très utilisé donc encore le moment de changer
       * TODO Cpm FAIT


   * mesure du CO2 :
       * [https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232](https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232)
       * « Informations hardware / énergétique — Avoir des infos sur le hardware utilisé pour les chatons qui s’auto-hébergent pourrait permettre de calculer les fameux ratio CO2 que les hébergeurs sont censés présentés depuis janvier 2022. »
       * connexe, inattendu, intéressant, nouveau
       * principe : reprendre l'indicateur tel quel dans un champ
       * TODO trouver des infos sur la réglementation


   * indication/détection de Yunhost
       * un nouveau champ ? host.os ? host.distribution ?


   * service.registration
       * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/service.properties#L67](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/service.properties#L67)
       * Free : inscription nécessaire mais ouverte à tout le monde et gratuite.
       * lors d'un atelier, des participants ont été troublés par « nécessaire »
       * le retirer ou le garder ?


   * service.registration.load :
       * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/service.properties#L72](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/service.properties#L72)
       * lors d'un atelier, des participants ont été troublés par la valeur « FREE »
       * idée ?


   * ajout de organization.type ?
       * ASSOCIATION
       * COLLECTIVE
       * COOPERATIVE
       * MICROENTERPRISE
       * ENTERPRISE
       * INDIVIDUAL
       * OTHER


   * fiche properties hébergement
       * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/hosting.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/hosting.properties)
           * la question se pose également pour la section host normale
               * d'ailleurs le cas vps.* est traitable dans host.*
               * recenser les systèmes d'exploitation des vm et des hosts ?
                   * host.os
               * recenser les systèmes d'hypervision des vm et des hosts ?
                   * host.hypervisor
               * service.os // hosting.os : compliqué à exprimer / ne correspond pas vraiment
           * on collecte déjà beaucoup d'informations,  est-ce bien nécessaire ?
               * permettrait de faire se questionner sur le logiciel de supervision utilisé
       * projection d'intégration dans stats.chatons.org
           * besoin d'un bouton « Services dédiés » 
           * certainement renommer « Services » en « Services mutualisés » +1
           * page d'index : 
               * ajouter un nombre spécifique pour les services dédiés
               * ajouter un indicateur pour différencier
               * dans le tableau : un colonne pour les services dédiés / une pour les services mutualisés
           * page d'un membre :
               * ajouter un nombre spécifique pour les services dédiés
               * ajouter un indicateur pour différencier
           * nommage : service mutualisé vs service dédié
           * faire des essais
               * TODO Angie FAIT : créer une fiche "fausse" fiche hosting.properties pour Framasoft afin de tester
                   * [https://framagit.org/framasoft/chatons/-/blob/main/Fichiers\_properties/hosting-nextcloud.properties](https://framagit.org/framasoft/chatons/-/blob/main/Fichiers\_properties/hosting-nextcloud.properties)
               * TODO Angie : demander à ljf de compléter une fiche hosting.properties pour l'une des offres d'hébergement qu'il propose
           * déjà 2 fichiers properties remplis !




## 59e réunion du groupe de travail

**jeudi 17 mars 2022 à 11h15**

Personnes présentes : Angie, Cpm, Jeey


### Divers

   * divers précédents :
       * création d'un schéma explicitant les subs
           * TODO Flo+Jeey
           * [https://excalidraw.com/#json=CKiP-SnK6uSXYvNIZHPP\_,CaVeIe\_gJNVMVDSbWdiqdA](~~https://excalidraw.com/#json=CKiP-SnK6uSXYvNIZHPP\_,CaVeIe\_gJNVMVDSbWdiqdA~~)
           * [https://excalidraw.com/#room=35f57e9f1613d69998b8,S3dvPNjcbOfEQ71VmigG3A](https://excalidraw.com/#room=35f57e9f1613d69998b8,S3dvPNjcbOfEQ71VmigG3A)
           * FAIT Upload du fichier schema-subs.svg sur la forge
       * demande d'amélioration de la doc# sur subs.foo (Zatalyz)
           * Voir conversation zatalys sur le forum et lui répondre
           * TODO Flo+Jeey
           * [https://forum.chatons.org/t/remarques-sur-organization-properties-et-service-properties/1736/2](https://forum.chatons.org/t/remarques-sur-organization-properties-et-service-properties/1736/2)
               * on peut répondre que c'est fait ! 
               * FAIT Jeey
           * TODO mettre le schéma dans le README
               * FAIT Mise à jour du README.md




### Revue de [https://stats.chatons.org/](https://stats.chatons.org/) 

   * page générique d'un chaton : 
       * penser à augmenter le code html avec les informations de properties pour faciliter le futur réagencement  UI/UX
           * TODO Cpm
   * page « Statistiques » (fédération) :
       * ajouter un donuts sur les services de paiement
           * TODO Cpm 
   * un jour peut-être :
       * pouvoir cliquer sur les graphiques pour voir la liste de résultats correspondant
           * par exemple pour les types d'inscription (à un service)
       * donuts sur les pays
           * pouvoir cliquer sur les résultats du camembert pour avoir une liste des chatons par pays
   * pages Uptimes (Federation, Organization, Services)
       * des améliorations à faire
           * TODO Cpm visibilité autres liens
           * TODO mrflos (pour l'été) : bidouiller la page statsuptime pour utiliser les filtres par état en js datatables
       * questions de statut manuel vs statut mesuré (page organization)
           * statut manuel seulement
           * statut mesuré seulement
           * les deux
           * un seul combiné des deux
           * discussion :
               * est-ce que la version manuelle est encore utile ? pertinence du mesuré
               * se poser la question de ce que cherche l'utilisateur
               * cas des statuts manuels « en travaux » ou  « fermé »
               * le statut manuel est plus important que le statut mesuré, respecté l'expression des admins
               * ne surtout pas afficher les deux
               * étudier la conjonction
           * TODO Cpm voir pour une version « combinée » avec bulle informative
   * Flo :
       * peut être avoir dans résumé des moyennes sans graphes, genre du texte « sur 2020 XXX visiteurs uniques, YYY ips différentes »
           * Cpm : une notion de « tendance » ?
           * Cpm : donner exemple ?
           * TODO Flo, à réfléchir l'enrichissement de texte des graphes toujours en TODO  > GT le 20 juillet > décalé
           * TODO Flo, à réfléchir à des cadres de tendances dans « Résumé » toujours en  > GT le 20 juillet > décalé
       * changer les intitulés « Web » et « Spécifique » par « Graphes de visites web » ou plus court « Graphes Web » et « Statistiques propres aux services » ou plus court « Stats des services » ?
           * Cpm : préciser l'intention
           * Flo : expliquer les items du menu type > GT le 20 juillet (annulé cause covid...)  > décalé
           * TODO Cpm : ajouter des bulles
           * TODO Flo : tester le menu métriques auprès de personnes
               * en cours toujours en TODO
           * TODO réfléchir
       * TODO Cpm afficher les champs nom et description des métrics dans les diagrammes
   * site statique vs site dynamique ?
       * différence de besoin entre Libre-service.eu (métrique au jour) et le collectif CHATONS (mérique au mois)
       * supprimer les vues années, semaines et jours ?
       * supprimer les périodes 12 mois, 2020 et 2021 ?
       * site statique :
           * avantages : simplicité de déploiement (est-ce nécessaire ?)
           * inconvénients : prend de la place sur disque
       * pour site dynamique :
           * avantages : plus grande interactivité, plus de liberté fonctionnelles
           * inconvénients : nécessite l'installation d'un serveur d'application Java, du travail pour finaliser
       * avis :
           * Antoine : pas de problème au site dynamique
           * Flo : peut-être finir les fonctionnalités en cours avant de faire une version 2 dynamique avec des optimisations
   * Test disponibilité en ipv6 :
       * [https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232](https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232)
       * est-ce une information déclarative ou à calculer ?
           * mieux si c'est calculé car plus à jour et un truc en moins à faire pour les admins
       * sujet non urgent, à approfondir
       * continuer à évaluer les besoins
       * page ipv6 pour avoir un aperçu du point de vue du collectif
   * Vérifier la présence de trackers basic. 
       * [https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232](https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232)
       * oui, très pertinent
       * valorisation : à envisager page dédiée
   * page logiciels :
       * emoji, tri, userCount, visitCount
       * TODO Cpm FAIT 🤩
   * page catégories :
       * emoji, tri, userCount, visitCount
       * TODO Cpm FAIT 🤩


### Revue générateur de fichiers métrics (statoolinfos)

   * ajout support Jitsi :
       * TODO Cpm en cours
   * ajout support Minetest
       * TODO Cpm 
   * ajout support PrivateBin
       * TODO Cpm en cours
   * ajout support Gitea
       * TODO Cpm en cours
   * ajout support Etherpad
   * ajout support Nextcloud
   * ajout support XMPP
   * metrics http
       * améliorer détection OS TODO Cpm
       * améliorer détection date TODO Cpm
   * pour info: mrflos travaille sur scrumblr et pourra faire la page stat
       * \o/


### Revue des catégories

   *  ([https://stats.chatons.org/category-autres.xhtml)](https://stats.chatons.org/category-autres.xhtml)) :
   * catégorie Agenda :
       * actuellement : Nextcloud Calendar, Radicale, SOgo, Tracim
       * rajouter Nextcloud ?
       * TODO Angie : check sur les modules par défaut sur NC et ajouter en fonction "Nextcloud"
   * rajouter Baïkal ?
       * site web : [https://sabre.io/baikal/](https://sabre.io/baikal/)
       * sources : [https://github.com/sabre-io/Baikal](https://github.com/sabre-io/Baikal)
       * licence : GNU GPLv3
       * source licence : [https://github.com/sabre-io/Baikal/blob/master/LICENSE](https://github.com/sabre-io/Baikal/blob/master/LICENSE)
       * catégorie : agenda ?
       * TODO Angie MAJ du fichier categories.properties sur le dépôt
       * TODO Cpm : à déployer FAIT
   * PeerJS Server
       * "A server side element to broker connections between PeerJS clients."
       * service aux utilisateurs ?
       * TODO Angie : vérifier et ajouter / demander la suppression de la fiche
   * ajout d'une catégorie « Gestionnaire de signature électronique »
       * nouvelle catégorie ? Oui.
       * logiciel :  [https://github.com/24eme/signaturepdf](https://github.com/24eme/signaturepdf) (proposé par libreon.fr)


### Revue des fichiers properties de membres

   * passer en revue :
       *  [https://stats.chatons.org/chatons-crawl.xhtml](https://stats.chatons.org/chatons-crawl.xhtml)
           * plus tard
       * [https://stats.chatons.org/chatons-propertyalerts.xhtml](https://stats.chatons.org/chatons-propertyalerts.xhtml)
           * 289 ~~292~~ ~~316~~ rouges, 1217 ~~1220~~ ~~1226~~ jaunes, communiquer un jour prochain (au moment de la prochaine release - celle avec les metrics)
   * message privé de Caracos pour ajout URL chatons.properties
       * TODO Cpm FAIT
   * [https://stats.chatons.org/devloprog-cloud.xhtml](https://stats.chatons.org/devloprog-cloud.xhtml)
       * service.registration = None
       * TODO Angie Contacter pour lui indiquer que le champs est mal rempli ("sans inscription" pour un NC)
   * Duchesse + les futurs radiés
       * [https://stats.chatons.org/duchesse-propertycheck.xhtml](https://stats.chatons.org/duchesse-propertycheck.xhtml)
       * date d'entrée dans le collectif ?
       * TODO Angie


### Revue des tickets

   * [https://framagit.org/chatons/chatonsinfos/-/issues/1](https://framagit.org/chatons/chatonsinfos/-/issues/1)
       * Redesign des encarts au dessus des tableaux
       * prévu lorsqu'on aura toutes les informations affichées
       * statut : plus tard
   * [https://framagit.org/chatons/chatonsinfos/-/issues/2](https://framagit.org/chatons/chatonsinfos/-/issues/2)
       * Dans le tableau des services de la fiche organisation des chatons, supprimer la colonne "Organisation"
       * Cpm : ce tableau est une vue mutualisée entre plusieurs pages : organisation, services, catégorie,  logiciel ; l'information est effectivement redondante pour la page organisation, mais ça permet de conserver l'homogénéité de la vue.
       * Cpm : pour gagner de la place, possibilité de ne mettre que le logo de l'organisation et le nom en bulle
       * statut : réfléchir et sinon sera traité par la grande revue visuelle prévue un jour
   * [https://framagit.org/chatons/chatonsinfos/-/issues/4](https://framagit.org/chatons/chatonsinfos/-/issues/4)
       * Penser un nouveau fichier properties dédié aux offres non logicielles, celle d'hébergement
       * statut : priorité aux services utilisateurs donc pertinent mais plus tard


### Revue des merge requests

   * [https://framagit.org/framasoft/chatons/-/merge\_requests](https://framagit.org/framasoft/chatons/-/merge\_requests)
   * RAS


### Revue du forum

   * [https://forum.chatons.org/c/collectif/stats-chatons-org/83](https://forum.chatons.org/c/collectif/stats-chatons-org/83)
   *  Quels sont les services les plus fréquentés ?
       * [https://forum.chatons.org/t/quels-sont-les-services-les-plus-frequentes-free-software-australia-veut-se-developper/3298/6](https://forum.chatons.org/t/quels-sont-les-services-les-plus-frequentes-free-software-australia-veut-se-developper/3298/6)
       * TODO Cpm FAIT


### Revue des disponibilités des services (uptimes)

   * [https://stats.chatons.org/chatons-uptimes.xhtml](https://stats.chatons.org/chatons-uptimes.xhtml)
   * Limesurvey  Nomagic
       * ~~rouge, à surveiller pour la prochaine fois~~
       * redevenu normal
   * Wekan      Picasoft
       * ~~rouge, à surveiller pour la prochaine fois~~
       * redevenu normal
   * Salons JabberFR [admins@jabberfr.org]
       * intermittent, à surveiller pour la prochaine fois (mais c'est plutôt leur site web que le service lui-même)
   * ValiseChaprilOrg
       * intermittent, à surveiller pour la prochaine fois


### Revue de l'ontologie

   * rappels :
       * l'ordre des questions à se poser : préfixe, sous-préfixe
       * on rajout .count si le mot signifiant est le préfixe
       * métriques HTTP :
           * contexte :
               * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties)
               * [http://www.webalizer.org/webalizer\_help.html](http://www.webalizer.org/webalizer\_help.html)
                   * [https://web.archive.org/web/20210330151216/http://www.webalizer.org/webalizer\_help.html](https://web.archive.org/web/20210330151216/http://www.webalizer.org/webalizer\_help.html)


   * metrics.barcodes.count -> metrics.barcodes.created/generated
       * explicitation
       * évite le terme « count »
       * avis : oui
       * TODO metrics.properties  FAIT par angie
       * TODO générateur métrics Cpm FAIT


   * mesure du CO2 :
       * [https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232](https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232)
       * « Informations hardware / énergétique — Avoir des infos sur le hardware utilisé pour les chatons qui s’auto-hébergent pourrait permettre de calculer les fameux ratio CO2 que les hébergeurs sont censés présentés depuis janvier 2022. »
       * connexe, inattendu, intéressant, nouveau
       * principe : reprendre l'indicateur tel quel dans un champ
       * TODO trouver des infos sur la réglementation
           * [https://www.ademe.fr/expertises/consommer-autrement/passer-a-laction/reconnaitre-produit-plus-respectueux-lenvironnement/dossier/laffichage-environnemental/affichage-environnemental-secteur-numerique](https://www.ademe.fr/expertises/consommer-autrement/passer-a-laction/reconnaitre-produit-plus-respectueux-lenvironnement/dossier/laffichage-environnemental/affichage-environnemental-secteur-numerique)
           * [http://www.base-impacts.ademe.fr/documents/Numerique.zip](http://www.base-impacts.ademe.fr/documents/Numerique.zip)
           * [https://base-impacts.ademe.fr/gestdoclist](https://base-impacts.ademe.fr/gestdoclist)
           * [https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000044327272](https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000044327272)
       * dépasse le gdt stats.chatons.org
       * vrai enjeu sur l'énergie et le co2
       * défi de trouver une formule magique qui soit utilisable, voire déployée dans le noyau GNU/Linux
       * ça serait bien d'avoir un gdt dédié


   * indication/détection de Yunohost
       * un nouveau champ ? ~~host.os ~~? host.distribution ?
       * le champ host.distribution a du sens
       * []~~optionnel~~[] vs ~~recommandé~~ vs obligatoire
       * Nom générique de la distribution installée sur le serveur (type STRING, obligatoire, ex. YunoHost)
       * host.distribution = f
       * TODO rajouter à tous les modèles de service et hosting Cpm


   * service.registration
       * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/service.properties#L67](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/service.properties#L67)
       * Free : inscription nécessaire mais ouverte à tout le monde et gratuite.
       * lors d'un atelier, des participants ont été troublés par « nécessaire »
       * le retirer ou le garder ?


   * service.registration.load :
       * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/service.properties#L72](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/service.properties#L72)
       * lors d'un atelier, des participants ont été troublés par la valeur « FREE »
       * idée ?


   * ajout de organization.type ?
       * ASSOCIATION
       * COLLECTIVE
       * COOPERATIVE
       * MICROENTERPRISE
       * ENTERPRISE
       * INDIVIDUAL
       * OTHER


   * fiche properties hébergement
       * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/hosting.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/hosting.properties)
           * la question se pose également pour la section host normale
               * d'ailleurs le cas vps.* est traitable dans host.*
               * recenser les systèmes d'exploitation des vm et des hosts ?
                   * host.os
               * recenser les systèmes d'hypervision des vm et des hosts ?
                   * host.hypervisor
               * service.os // hosting.os : compliqué à exprimer / ne correspond pas vraiment
           * on collecte déjà beaucoup d'informations,  est-ce bien nécessaire ?
               * permettrait de faire se questionner sur le logiciel de supervision utilisé
       * projection d'intégration dans stats.chatons.org
           * besoin d'un bouton « Services dédiés » 
           * certainement renommer « Services » en « Services mutualisés » +1
           * page d'index : 
               * ajouter un nombre spécifique pour les services dédiés
               * ajouter un indicateur pour différencier
               * dans le tableau : un colonne pour les services dédiés / une pour les services mutualisés
           * page d'un membre :
               * ajouter un nombre spécifique pour les services dédiés
               * ajouter un indicateur pour différencier
           * nommage : service mutualisé vs service dédié
           * faire des essais
               * TODO Angie FAIT : créer une fiche "fausse" fiche hosting.properties pour Framasoft afin de tester
                   * [https://framagit.org/framasoft/chatons/-/blob/main/Fichiers\_properties/hosting-nextcloud.properties](https://framagit.org/framasoft/chatons/-/blob/main/Fichiers\_properties/hosting-nextcloud.properties)
               * TODO Angie : demander à ljf de compléter une fiche hosting.properties pour l'une des offres d'hébergement qu'il propose
           * déjà 2 fichiers properties remplis !


## 60e réunion du groupe de travail

**jeudi 07 avril 2022 à 11h15**

Personnes présentes : Jérémy, Cpm

### Revue de [https://stats.chatons.org/](https://stats.chatons.org/) 

   * page générique d'un chaton : 
       * penser à augmenter le code html avec les informations de properties pour faciliter le futur réagencement  UI/UX
           * TODO Cpm
   * page « Statistiques » (fédération) :
       * ajouter un donuts sur les services de paiement
           * TODO Cpm 
   * un jour peut-être :
       * pouvoir cliquer sur les graphiques pour voir la liste de résultats correspondant
           * par exemple pour les types d'inscription (à un service)
       * donuts sur les pays
           * pouvoir cliquer sur les résultats du camembert pour avoir une liste des chatons par pays
   * pages Uptimes (Federation, Organization, Services)
       * des améliorations à faire
           * TODO Cpm visibilité autres liens
           * TODO mrflos (pour l'été) : bidouiller la page statsuptime pour utiliser les filtres par état en js datatables
       * questions de statut manuel vs statut mesuré (page organization)
           * statut manuel seulement
           * statut mesuré seulement
           * les deux
           * un seul combiné des deux
           * discussion :
               * est-ce que la version manuelle est encore utile ? pertinence du mesuré
               * se poser la question de ce que cherche l'utilisateur
               * cas des statuts manuels « en travaux » ou  « fermé »
               * le statut manuel est plus important que le statut mesuré, respecté l'expression des admins
               * ne surtout pas afficher les deux
               * étudier la conjonction
           * TODO Cpm voir pour une version « combinée » avec bulle informative
   * Flo :
       * peut être avoir dans résumé des moyennes sans graphes, genre du texte « sur 2020 XXX visiteurs uniques, YYY ips différentes »
           * Cpm : une notion de « tendance » ?
           * Cpm : donner exemple ?
           * TODO Flo, à réfléchir l'enrichissement de texte des graphes toujours en TODO  > GT le 20 juillet > décalé
           * TODO Flo, à réfléchir à des cadres de tendances dans « Résumé » toujours en  > GT le 20 juillet > décalé
       * changer les intitulés « Web » et « Spécifique » par « Graphes de visites web » ou plus court « Graphes Web » et « Statistiques propres aux services » ou plus court « Stats des services » ?
           * Cpm : préciser l'intention
           * Flo : expliquer les items du menu type > GT le 20 juillet (annulé cause covid...)  > décalé
           * TODO Cpm : ajouter des bulles
           * TODO Flo : tester le menu métriques auprès de personnes
               * en cours toujours en TODO
           * TODO réfléchir
       * TODO Cpm afficher les champs nom et description des métrics dans les diagrammes
   * site statique vs site dynamique ?
       * différence de besoin entre Libre-service.eu (métrique au jour) et le collectif CHATONS (mérique au mois)
       * supprimer les vues années, semaines et jours ?
       * supprimer les périodes 12 mois, 2020 et 2021 ?
       * site statique :
           * avantages : simplicité de déploiement (est-ce nécessaire ?)
           * inconvénients : prend de la place sur disque
       * pour site dynamique :
           * avantages : plus grande interactivité, plus de liberté fonctionnelles
           * inconvénients : nécessite l'installation d'un serveur d'application Java, du travail pour finaliser
       * avis :
           * Antoine : pas de problème au site dynamique
           * Flo : peut-être finir les fonctionnalités en cours avant de faire une version 2 dynamique avec des optimisations
   * Test disponibilité en ipv6 :
       * [https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232](https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232)
       * est-ce une information déclarative ou à calculer ?
           * mieux si c'est calculé car plus à jour et un truc en moins à faire pour les admins
       * sujet non urgent, à approfondir
       * continuer à évaluer les besoins
       * page ipv6 pour avoir un aperçu du point de vue du collectif
   * Vérifier la présence de trackers basic. 
       * [https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232](https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232)
       * oui, très pertinent
       * valorisation : à envisager page dédiée
   * valeurs fausses du metrics visiteurs de pyStatoolInfos
       * en mars : 140M de visiteurs pour 7M de visites
       * TODO Contacter Sleto : CPM FAITx2
       * TODO attendre retour
       * TODO Angie : changer de générateur ? autre ?


### Revue générateur de fichiers métrics (statoolinfos)

   * ajout support Jitsi :
       * TODO Cpm en cours
   * ajout support Minetest
       * TODO Cpm FAIT
   * ajout support PrivateBin
       * TODO Cpm FAIT
   * ajout support Gitea
       * TODO Cpm FAIT
   * ajout support Etherpad
       * TODO Cpm FAIT
   * ajout support Nextcloud
   * ajout support XMPP
   * metrics http
       * améliorer détection OS TODO Cpm
       * améliorer détection date TODO Cpm
   * pour info: mrflos travaille sur scrumblr et pourra faire la page stat
       * \o/


### Revue des catégories

   *  ([https://stats.chatons.org/category-autres.xhtml)](https://stats.chatons.org/category-autres.xhtml)) :
   * catégorie Agenda :
       * actuellement : Nextcloud Calendar, Radicale, SOgo, Tracim
       * rajouter Nextcloud ?
       * TODO Angie : check sur les modules par défaut sur NC et ajouter en fonction "Nextcloud"
       * Apps par défaut : Fichiers, Activité, Photos
       * Nextcloud propose dès l'installation d'ajouter les applications principales suivantes : ~~Agenda~~ Calendar, Contacts, Mail, Talk, Collabora (maintenant Nextcloud Office) et son backend intégré
       * Nextcloud ajouté dans categories.properties
           * possible erreur mais a priori très rare ou impropable
           * tous les Nextcloud sont-ils installés avec Calendar ?
           * TODO réfléchir si l'erreur rare est acceptable ou confusante
   * PeerJS Server
       * "A server side element to broker connections between PeerJS clients."
       * service aux utilisateurs ?
       * TODO Angie : vérifier et ajouter / demander la suppression de la fiche
       * FAIT Angie : demande de précisions à Garbaye (est-ce un service ? si oui, quelle catégorie ?)
       * Réponse : 
       * *C’est un outil qui est bien ouvert à toute utilisation (accès public), mais plutôt pour permettre la construction de sites et outils web s’appuyant dessus.*
       * *Ce qu’il fait si j’arrive à le le rendre simple : PeerJS permet de connecter en pair à pair deux (ou +) navigateurs web, pour y échanger des flux audio ou vidéo. Ceci permet de créer en quelques lignes de code javascript un système de visio-conférence (tous vers tous) ou de diffusion (un vers tous) très léger et simple.*
       * *Un ami dans l’enseignement supérieur l’a utilisé avec succès au début du premier confinement pour donner des cours à distance. Je pense qu’il n’en a plus besoin mais nous l’avons gardé par principe.*
       * *Je le vois mal rentrer dans une catégorie CHATONS, c’est vraiment un service de niche, et il ne remplace aucun outil privateur équivalent. On peut arrêter de l’annoncer, si il est impératif qu’il rentre dans une catégorie (je ne vois pas de solution, c’est déjà pas simple de décrire ce qu’il fait en quelque mots!).*
       * *Je m’interroge, est-ce qu’un CHATONS peut porter des services d’infrastructure à destination d’un public plus technique, je pense pour les plus connus à :*
       * *NTP (serveur de temps internet, fonctionne en fédération si on peut dire)*
       * *DNS (résolution de nom de domaine) et ses variantes sécurisées DoH/DoT qui sont des outils pour garantie la neutralité du Net.*
       * *Serveurs STUN/TURN (pas de souci si c’est du charabia  )*
       * *Services pour trouver son adresse IP publique (comme *[https://ifconfig.co/](*https://ifconfig.co/*)* )*
*J’avais regardé les autres CHATONS et ils ne semblaient pas en proposer. Ces briques sont aujourd’hui plutôt portées par des associations autour de la FFDN.*

*Est-il préférable si on veut les proposer de le faire sous une autre forme que le CHATONS, et garder les services offerts sous le nom du CHATONS « tout public » ?*

       * donc il y a bien un service qui est rendu aux utilisateurs
       * autres exemples de services similaires
           * Squid (proxy web)
           * Nitter
           * Invidious
       * reste à trouver un nom de catégorie : proxy vs Relai de flux 
   * ajout d'une catégorie « Gestionnaire de signature électronique »
       * nouvelle catégorie ? Oui.
       * logiciel :  [https://github.com/24eme/signaturepdf](https://github.com/24eme/signaturepdf) (proposé par libreon.fr)
       * FAIT / Ajout de : 
           * *#Gestionnaire de signature électronique*
           * categories.electronicsignature.name=Gestionnaire de signature électronique
           * categories.electronicsignature.description=
           * categories.electronicsignature.logo=esignature.svg
           * categories.electronicsignature.softwares=signaturepdf, Signature PDF
   * Open edX \& Weblate
       * [https://forum.chatons.org/t/ajout-de-open-edx-weblate-sur-chatons-org/3417](https://forum.chatons.org/t/ajout-de-open-edx-weblate-sur-chatons-org/3417)
       * autres logiciels à y ranger : Moodle, YesWiki
       * trouver un nom de catégorie :
           * plateforme d'apprentissage +++
           * espace numérique d'apprentissage
       * TODO décider nom + propager


### Revue des fichiers properties de membres

   * passer en revue :
       *  [https://stats.chatons.org/chatons-crawl.xhtml](https://stats.chatons.org/chatons-crawl.xhtml)
           * plus tard
       * [https://stats.chatons.org/chatons-propertyalerts.xhtml](https://stats.chatons.org/chatons-propertyalerts.xhtml)
           * 498 ~~284~~ ~~289 ~~ ~~292~~  ~~316~~ rouges, 1209 ~~1220~~  ~~1217~~  ~~1220~~  ~~1226~~ jaunes, communiquer un jour prochain (au moment de la prochaine release - celle avec les metrics)
               * augmentation due à l'ajout d'une nouvelle propriétés non remplie pour le moment -> à suivre...
   * [https://stats.chatons.org/devloprog-cloud.xhtml](https://stats.chatons.org/devloprog-cloud.xhtml)
       * service.registration = None
       * TODO Angie Contacter pour lui indiquer que le champs est mal rempli ("sans inscription" pour un NC)
       * FAIT le 6/04 / modification effectuée dans la foulée
   * Organisations radiées
       * organization.status.level=AWAY
       * organization.memberof.chatons.enddate = 01/04/2022
       * il ne devrait donc plus rester de organization.status.level=IDLE (seulement ACTIVE)
   * Ajout manuel du fichier organization.properties de Liberta le 06/04 (reçu par mail sur contact@chatons.org)
   * rapprochement avec fiches chatons.org (prospective) :
       * constat qu'il y a eu une convergence de la fiche service sur chatons.org et le fichier properties de service
       * comment pourrait s'envisager une étape supplémentaire de rapprochement ?
       * niveau organisation :
           * si on complète le formulaire de la fiche structure (?) sur chatons.org alors chatons.org serait à même de générer automatiquement le fichier properties de l'organisation
               * Si on automatise à la création d'une fiche, on va se retrouver avec les fiches des candidats chatons et autres huluberlus qui créent des fiches sans jamais candidater. Il faudrait donc que la génération n'intervienne que lorsqu'on modifie la valeur du champ `Etat de la candidature` en `chaton approuvé`    +1 +1
           * avantages :
               * un plus grand nombre d'organisations suivies dans stats.chatons.org
               * une gestion automatique
           * inconvénients :
               * besoin d'étudier des cas (suppression de fiche structure, sortie du collectif, etc.)
       *  niveau service :
           * si on complète le formulaire de la fiche service sur chatons.org alors chatons.org serait à même de générer automatiquement le fichier properties de service
               * comment la génération automatique de ces fiches service.properties viendront-elles alimenter la section Subs de fiches organization.properties ?
                   * très important oui
           * avantages :
               *  un plus grand nombre de services dans stats.chatons.org
               * une gestion automatique
           * besoin d'étudier des cas (suppression de fiche service, sortie du collectif, etc.)
           * Premier pas :
               * Rassembler les infos :
                   * Le nom de l'information et où se trouve-t-elle (si ça se trouve ? et où : fiche Chatons ? Properties) ?


### Revue des tickets

   * [https://framagit.org/chatons/chatonsinfos/-/issues/1](https://framagit.org/chatons/chatonsinfos/-/issues/1)
       * Redesign des encarts au dessus des tableaux
       * prévu lorsqu'on aura toutes les informations affichées
       * statut : plus tard
   * [https://framagit.org/chatons/chatonsinfos/-/issues/2](https://framagit.org/chatons/chatonsinfos/-/issues/2)
       * Dans le tableau des services de la fiche organisation des chatons, supprimer la colonne "Organisation"
       * Cpm : ce tableau est une vue mutualisée entre plusieurs pages : organisation, services, catégorie,  logiciel ; l'information est effectivement redondante pour la page organisation, mais ça permet de conserver l'homogénéité de la vue.
       * Cpm : pour gagner de la place, possibilité de ne mettre que le logo de l'organisation et le nom en bulle
       * statut : réfléchir et sinon sera traité par la grande revue visuelle prévue un jour
   * [https://framagit.org/chatons/chatonsinfos/-/issues/4](https://framagit.org/chatons/chatonsinfos/-/issues/4)
       * Penser un nouveau fichier properties dédié aux offres non logicielles, celle d'hébergement
       * statut : priorité aux services utilisateurs donc pertinent mais plus tard


### Revue des merge requests

   * [https://framagit.org/framasoft/chatons/-/merge\_requests](https://framagit.org/framasoft/chatons/-/merge\_requests)
   * [https://framagit.org/chatons/chatonsinfos/-/merge\_requests/45](https://framagit.org/chatons/chatonsinfos/-/merge\_requests/45)
       * ajout de modèles pour les logiciels LSTU \& LUFI 
       * FAIT Angie


### Revue du forum

   * [https://forum.chatons.org/c/collectif/stats-chatons-org/83](https://forum.chatons.org/c/collectif/stats-chatons-org/83)
   * annonce nouveau champ host.server.distribution
       * TODO Cpm FAIT
   * RAS


### Revue des disponibilités des services (uptimes)

   * [https://stats.chatons.org/chatons-uptimes.xhtml](https://stats.chatons.org/chatons-uptimes.xhtml)
   * Salons JabberFR [admins@jabberfr.org]
       * intermittent, à surveiller pour la prochaine fois (mais c'est plutôt leur site web que le service lui-même)
       * réponse de mathieui :
           * oui on a des sautes de réseau (2 grosses aujourd’hui)
           * faut qu’on regarde si c’est de notre côté et sinon ouvrir un ticket chez l’hébergeur
       * réponse de LinkMauve :
           * Je viens de réduire le temps de chargement de [https://jabberfr.org](https://jabberfr.org) de 3~5s à 0,06~0,08s. :)
       * résultat : 
   * ValiseChaprilOrg
       * intermittent, à surveiller pour la prochaine fois
       * post mastodon pb identifier maintenance en cours
   * Visio Colibri
       * TODO Jeey contacter :*>
   * Sickurl
       * TODO Jeey contacter 


### Revue de l'ontologie

   * rappels :
       * l'ordre des questions à se poser : préfixe, sous-préfixe
       * on rajout .count si le mot signifiant est le préfixe
       * métriques HTTP :
           * contexte :
               * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties)
               * [http://www.webalizer.org/webalizer\_help.html](http://www.webalizer.org/webalizer\_help.html)
                   * [https://web.archive.org/web/20210330151216/http://www.webalizer.org/webalizer\_help.html](https://web.archive.org/web/20210330151216/http://www.webalizer.org/webalizer\_help.html)


   * mesure du CO2 :
       * proposer un gdt dédié à ce sujet lors de la prochaine réunion mensuelle


   * host.server.distribution
       * afin de répondre au besoin de détection de Yunohost
       * ~~host.distribution~~ vs host.server.distribution
           * nous avons déjà host.server.type
       * Nom générique de la distribution installée sur le serveur (type STRING, obligatoire, ex. YunoHost)
       * host.server.distribution = 
       * TODO rajouter à tous les modèles de service et hosting Cpm FAIT
       * TODO rajouter la règle de vérification dans StatoolInfos Cpm FAIT
       * TODO rajouter le graphic dans StatoolInfos Cpm FAIT


   * service.registration
       * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/service.properties#L67](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/service.properties#L67)
       * Free : inscription nécessaire mais ouverte à tout le monde et gratuite.
       * lors d'un atelier, des participants ont été troublés par « nécessaire »
       * le retirer ou le garder ?


   * service.registration.load :
       * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/service.properties#L72](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/service.properties#L72)
       * lors d'un atelier, des participants ont été troublés par la valeur « FREE »
       * idée ?


   * ajout de organization.type ?
       * ASSOCIATION
       * COLLECTIVE
       * COOPERATIVE
       * MICROENTERPRISE
       * ENTERPRISE
       * INDIVIDUAL
       * OTHER


   * fiche properties hébergement
       * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/hosting.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/hosting.properties)
           * la question se pose également pour la section host normale
               * d'ailleurs le cas vps.* est traitable dans host.*
               * recenser les systèmes d'exploitation des vm et des hosts ?
                   * host.os
               * recenser les systèmes d'hypervision des vm et des hosts ?
                   * host.hypervisor
               * service.os // hosting.os : compliqué à exprimer / ne correspond pas vraiment
           * on collecte déjà beaucoup d'informations,  est-ce bien nécessaire ?
               * permettrait de faire se questionner sur le logiciel de supervision utilisé
       * projection d'intégration dans stats.chatons.org
           * besoin d'un bouton « Services dédiés » 
           * certainement renommer « Services » en « Services mutualisés » +1
           * page d'index : 
               * ajouter un nombre spécifique pour les services dédiés
               * ajouter un indicateur pour différencier
               * dans le tableau : un colonne pour les services dédiés / une pour les services mutualisés
           * page d'un membre :
               * ajouter un nombre spécifique pour les services dédiés
               * ajouter un indicateur pour différencier
           * nommage : service mutualisé vs service dédié
           * faire des essais
               * TODO Angie FAIT : créer une fiche "fausse" fiche hosting.properties pour Framasoft afin de tester
                   * [https://framagit.org/framasoft/chatons/-/blob/main/Fichiers\_properties/hosting-nextcloud.properties](https://framagit.org/framasoft/chatons/-/blob/main/Fichiers\_properties/hosting-nextcloud.properties)
               * TODO Angie : demander à ljf de compléter une fiche hosting.properties pour l'une des offres d'hébergement qu'il propose
           * déjà 2 fichiers properties remplis !


## 61e réunion du groupe de travail

**jeudi 21 avril 2022 à 11h15**

Personnes présentes : Angie / Cpm / MrFlos


### Revue de [https://stats.chatons.org/](https://stats.chatons.org/) 

   * page générique d'un chaton : 
       * penser à augmenter le code html avec les informations de properties pour faciliter le futur réagencement  UI/UX
           * TODO Cpm
   * page « Statistiques » (fédération) :
       * ajouter un donuts sur les services de paiement
           * TODO Cpm 
   * un jour peut-être :
       * pouvoir cliquer sur les graphiques pour voir la liste de résultats correspondant
           * par exemple pour les types d'inscription (à un service)
       * donuts sur les pays
           * pouvoir cliquer sur les résultats du camembert pour avoir une liste des chatons par pays
   * pages Uptimes (Federation, Organization, Services)
       * des améliorations à faire
           * TODO Cpm visibilité autres liens
           * TODO mrflos (pour l'été) : bidouiller la page statsuptime pour utiliser les filtres par état en js datatables
       * questions de statut manuel vs statut mesuré (page organization)
           * statut manuel seulement
           * statut mesuré seulement
           * les deux
           * un seul combiné des deux
           * discussion :
               * est-ce que la version manuelle est encore utile ? pertinence du mesuré
               * se poser la question de ce que cherche l'utilisateur
               * cas des statuts manuels « en travaux » ou  « fermé »
               * le statut manuel est plus important que le statut mesuré, respecté l'expression des admins
               * ne surtout pas afficher les deux
               * étudier la conjonction
           * TODO Cpm voir pour une version « combinée » avec bulle informative
   * Flo :
       * peut être avoir dans résumé des moyennes sans graphes, genre du texte « sur 2020 XXX visiteurs uniques, YYY ips différentes »
           * Cpm : une notion de « tendance » ?
           * Cpm : donner exemple ?
           * TODO Flo, à réfléchir l'enrichissement de texte des graphes toujours en TODO  > GT le 20 juillet > décalé
           * TODO Flo, à réfléchir à des cadres de tendances dans « Résumé » toujours en  > GT le 20 juillet > décalé
       * changer les intitulés « Web » et « Spécifique » par « Graphes de visites web » ou plus court « Graphes Web » et « Statistiques propres aux services » ou plus court « Stats des services » ?
           * Cpm : préciser l'intention
           * Flo : expliquer les items du menu type > GT le 20 juillet (annulé cause covid...)  > décalé
           * TODO Cpm : ajouter des bulles
           * TODO Flo : tester le menu métriques auprès de personnes
               * en cours toujours en TODO
           * TODO réfléchir
       * TODO Cpm afficher les champs nom et description des métrics dans les diagrammes
   * site statique vs site dynamique ?
       * différence de besoin entre Libre-service.eu (métrique au jour) et le collectif CHATONS (mérique au mois)
       * supprimer les vues années, semaines et jours ?
       * supprimer les périodes 12 mois, 2020 et 2021 ?
       * site statique :
           * avantages : simplicité de déploiement (est-ce nécessaire ?)
           * inconvénients : prend de la place sur disque
       * pour site dynamique :
           * avantages : plus grande interactivité, plus de liberté fonctionnelles
           * inconvénients : nécessite l'installation d'un serveur d'application Java, du travail pour finaliser
       * avis :
           * Antoine : pas de problème au site dynamique
           * Flo : peut-être finir les fonctionnalités en cours avant de faire une version 2 dynamique avec des optimisations
   * Test disponibilité en ipv6 :
       * [https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232](https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232)
       * est-ce une information déclarative ou à calculer ?
           * mieux si c'est calculé car plus à jour et un truc en moins à faire pour les admins
       * sujet non urgent, à approfondir
       * continuer à évaluer les besoins
       * page ipv6 pour avoir un aperçu du point de vue du collectif
   * Vérifier la présence de trackers basic. 
       * [https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232](https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232)
       * oui, très pertinent
       * valorisation : à envisager page dédiée
   * valeurs fausses du metrics visiteurs de pyStatoolInfos (utilisé par Sleto et Framasoft)
       * en mars : 140M de visiteurs pour 7M de visites
       * TODO Contacter Sleto : CPM FAITx2
       * TODO attendre retour
       * TODO Angie : changer de générateur ? autre ?


### Revue générateur de fichiers métrics (statoolinfos)

   * ajout support Jitsi :
       * TODO Cpm en cours
   * ajout support Nextcloud
   * ajout support XMPP
   * metrics http
       * améliorer détection OS TODO Cpm
       * améliorer détection date TODO Cpm
   * pour info: mrflos travaille sur scrumblr et pourra faire la page stat
       * \o/
   * todo mrflos : contacter tobias a propos de l'appli yunohost chatons FAIT le 21 avril


### Revue des catégories

   *  ([https://stats.chatons.org/category-autres.xhtml)](https://stats.chatons.org/category-autres.xhtml)) :
   * catégorie Agenda :
       * actuellement : Nextcloud Calendar, Radicale, SOgo, Tracim
       * rajouter Nextcloud ?
       * TODO Angie : check sur les modules par défaut sur NC et ajouter en fonction "Nextcloud"
       * Apps par défaut : Fichiers, Activité, Photos
       * Nextcloud propose dès l'installation d'ajouter les applications principales suivantes : ~~Agenda~~ Calendar, Contacts, Mail, Talk, Collabora (maintenant Nextcloud Office) et son backend intégré
       * Nextcloud ajouté dans categories.properties
           * possible erreur mais a priori très rare ou impropable
           * tous les Nextcloud sont-ils installés avec Calendar ? non, certains NC sont installés avec 1 seule App
           * Mais quasiment installé sur toutes les instances NC
           * TODO réfléchir si l'erreur rare est acceptable ou confusante
           * décision : OUI on assume 
           * Nextcloud est déjà dans les catégories Agenda et Contacts
       * FAIT
   * PeerJS Server
       * "A server side element to broker connections between PeerJS clients."
       * service aux utilisateurs ?
       * TODO Angie : vérifier et ajouter / demander la suppression de la fiche
       * FAIT Angie : demande de précisions à Garbaye (est-ce un service ? si oui, quelle catégorie ?)
       * Réponse : 
       * *C’est un outil qui est bien ouvert à toute utilisation (accès public), mais plutôt pour permettre la construction de sites et outils web s’appuyant dessus.*
       * *Ce qu’il fait si j’arrive à le le rendre simple : PeerJS permet de connecter en pair à pair deux (ou +) navigateurs web, pour y échanger des flux audio ou vidéo. Ceci permet de créer en quelques lignes de code javascript un système de visio-conférence (tous vers tous) ou de diffusion (un vers tous) très léger et simple.*
       * *Un ami dans l’enseignement supérieur l’a utilisé avec succès au début du premier confinement pour donner des cours à distance. Je pense qu’il n’en a plus besoin mais nous l’avons gardé par principe.*
       * *Je le vois mal rentrer dans une catégorie CHATONS, c’est vraiment un service de niche, et il ne remplace aucun outil privateur équivalent. On peut arrêter de l’annoncer, si il est impératif qu’il rentre dans une catégorie (je ne vois pas de solution, c’est déjà pas simple de décrire ce qu’il fait en quelque mots!).*
       * *Je m’interroge, est-ce qu’un CHATONS peut porter des services d’infrastructure à destination d’un public plus technique, je pense pour les plus connus à :*
       * *NTP (serveur de temps internet, fonctionne en fédération si on peut dire)*
       * *DNS (résolution de nom de domaine) et ses variantes sécurisées DoH/DoT qui sont des outils pour garantie la neutralité du Net.*
       * *Serveurs STUN/TURN (pas de souci si c’est du charabia  )*
       * *Services pour trouver son adresse IP publique (comme *[https://ifconfig.co/](*https://ifconfig.co/*)* )*
*J’avais regardé les autres CHATONS et ils ne semblaient pas en proposer. Ces briques sont aujourd’hui plutôt portées par des associations autour de la FFDN.*

*Est-il préférable si on veut les proposer de le faire sous une autre forme que le CHATONS, et garder les services offerts sous le nom du CHATONS « tout public » ?*

    * donc il y a bien un service qui est rendu aux utilisateurs
    * autres exemples de services similaires
        * Squid (proxy web)
        * Nitter
        * Invidious
        * bibliogram
    * reste à trouver un nom de catégorie : 
        * proxy ++ : serveur mandataire intermédiaire pouvant anonymiser les données d'Internet
        * Larousse : [https://www.larousse.fr/dictionnaires/francais/proxy/186673](https://www.larousse.fr/dictionnaires/francais/proxy/186673)
        * Dispositif informatique servant d'intermédiaire entre les ordinateurs d'un réseau privé et Internet. (Recommandation officielle : serveur mandataire.) [Il fait notamment office de *pare-feu* et de *cache*.] 
           * Relai de flux ---
           * interface web anonymisante
           * façade 
           * FAIT : création de la catégorie "Proxy"
           * TODO Angie : ajouter un picto
   * Open edX~~ \& Weblate~~
       * [https://forum.chatons.org/t/ajout-de-open-edx-weblate-sur-chatons-org/3417](https://forum.chatons.org/t/ajout-de-open-edx-weblate-sur-chatons-org/3417)
       * autres logiciels à y ranger : Moodle, Scenarii, YesWiki
       * trouver un nom de catégorie :
           * plateforme d'apprentissage +++
           * espace numérique d'apprentissage
           * espace de formation en ligne
           * en fait, ce type d'outils a un nom : LMS (Learning Management System). Pourquoi ne pas l'utiliser ?
           * plateforme de formation (LMS) ++++
               * nommage adopté
       * FAIT Angie enrichir le fichier categories.properties
       * TODO Angie : ajouter un picto lms.svg
       * TODO répondre dans le forum
   * ~~Open edX \& ~~Weblate
       * [https://forum.chatons.org/t/ajout-de-open-edx-weblate-sur-chatons-org/3417](https://forum.chatons.org/t/ajout-de-open-edx-weblate-sur-chatons-org/3417)
       * outil collaboratif de traduction de logiciels ou de sites webs
       * Categorie : outils de traductions
       * chemin : translator
       * FAIT Angie enrichir le fichier categories.properties
       * TODO Angie : ajouter un picto translator.svg
       * TODO répondre dans le forum 

### Revue des fichiers properties de membres

   * passer en revue :
       *  [https://stats.chatons.org/chatons-crawl.xhtml](https://stats.chatons.org/chatons-crawl.xhtml)
           * plus tard
       * [https://stats.chatons.org/chatons-propertyalerts.xhtml](https://stats.chatons.org/chatons-propertyalerts.xhtml)
           * 498 ~~284~~ ~~289 ~~ ~~292~~  ~~316~~ rouges, 1209 ~~1220~~  ~~1217~~  ~~1220~~  ~~1226~~ jaunes
           * communiquer un jour prochain (au moment de la prochaine release - celle avec les metrics)
   * rapprochement avec fiches chatons.org (prospective) :
       * constat qu'il y a eu une convergence de la fiche service sur chatons.org et le fichier properties de service
       * comment pourrait s'envisager une étape supplémentaire de rapprochement ?
       * niveau organisation :
           * si on complète le formulaire de la fiche structure (?) sur chatons.org alors chatons.org serait à même de générer automatiquement le fichier properties de l'organisation
               * Si on automatise à la création d'une fiche, on va se retrouver avec les fiches des candidats chatons et autres huluberlus qui créent des fiches sans jamais candidater. Il faudrait donc que la génération n'intervienne que lorsqu'on modifie la valeur du champ `Etat de la candidature` en `chaton approuvé`    +1 +1
           * avantages :
               * un plus grand nombre d'organisations suivies dans stats.chatons.org
               * une gestion automatique
           * inconvénients :
               * besoin d'étudier des cas (suppression de fiche structure, sortie du collectif, etc.)
       *  niveau service :
           * si on complète le formulaire de la fiche service sur chatons.org alors chatons.org serait à même de générer automatiquement le fichier properties de service
               * comment la génération automatique de ces fiches service.properties viendront-elles alimenter la section Subs de fiches organization.properties ?
                   * très important oui
           * avantages :
               *  un plus grand nombre de services dans stats.chatons.org
               * une gestion automatique
           * besoin d'étudier des cas (suppression de fiche service, sortie du collectif, etc.)
           * Premier pas :
               * Rassembler les infos :
                   * Le nom de l'information et où se trouve-t-elle (si ça se trouve ? et où : fiche Chatons ? Properties) ?
       * TODO recensement comparatif + table de concordance, Angie \o/

### Revue des tickets

   * [https://framagit.org/chatons/chatonsinfos/-/issues/1](https://framagit.org/chatons/chatonsinfos/-/issues/1)
       * Redesign des encarts au dessus des tableaux
       * prévu lorsqu'on aura toutes les informations affichées
       * statut : plus tard
   * [https://framagit.org/chatons/chatonsinfos/-/issues/2](https://framagit.org/chatons/chatonsinfos/-/issues/2)
       * Dans le tableau des services de la fiche organisation des chatons, supprimer la colonne "Organisation"
       * Cpm : ce tableau est une vue mutualisée entre plusieurs pages : organisation, services, catégorie,  logiciel ; l'information est effectivement redondante pour la page organisation, mais ça permet de conserver l'homogénéité de la vue.
       * Cpm : pour gagner de la place, possibilité de ne mettre que le logo de l'organisation et le nom en bulle
       * statut : réfléchir et sinon sera traité par la grande revue visuelle prévue un jour
   * [https://framagit.org/chatons/chatonsinfos/-/issues/4](https://framagit.org/chatons/chatonsinfos/-/issues/4)
       * Penser un nouveau fichier properties dédié aux offres non logicielles, celle d'hébergement
       * statut : priorité aux services utilisateurs donc pertinent mais plus tard


### Revue des merge requests

   * [https://framagit.org/framasoft/chatons/-/merge\_requests](https://framagit.org/framasoft/chatons/-/merge\_requests)


### Revue du forum

   * [https://forum.chatons.org/c/collectif/stats-chatons-org/83](https://forum.chatons.org/c/collectif/stats-chatons-org/83)
   * RAS


### Revue des disponibilités des services (uptimes)

   * [https://stats.chatons.org/chatons-uptimes.xhtml](https://stats.chatons.org/chatons-uptimes.xhtml)
   * Salons JabberFR [admins@jabberfr.org]
       * intermittent, à surveiller pour la prochaine fois (mais c'est plutôt leur site web que le service lui-même)
       * réponse de mathieui :
           * oui on a des sautes de réseau (2 grosses aujourd’hui)
           * faut qu’on regarde si c’est de notre côté et sinon ouvrir un ticket chez l’hébergeur
       * réponse de LinkMauve :
           * Je viens de réduire le temps de chargement de [https://jabberfr.org](https://jabberfr.org) de 3~5s à 0,06~0,08s. :)
       * résultat : 
   * ValiseChaprilOrg
       * intermittent, à surveiller pour la prochaine fois
       * post mastodon pb identifié maintenance en cours
   * Colibri > Visio
       * en cours de redéploiement
   * Sleto > Equipe
       * TODO Jeey/MrFlos contacter
   * SiickServices > Siickurl
       * TODO Jeey contacter


### Revue de l'ontologie

   * rappels :
       * l'ordre des questions à se poser : préfixe, sous-préfixe
       * on rajout .count si le mot signifiant est le préfixe
       * métriques HTTP :
           * contexte :
               * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties)
               * [http://www.webalizer.org/webalizer\_help.html](http://www.webalizer.org/webalizer\_help.html)
                   * [https://web.archive.org/web/20210330151216/http://www.webalizer.org/webalizer\_help.html](https://web.archive.org/web/20210330151216/http://www.webalizer.org/webalizer\_help.html)


   * mesure du CO2 :
       * proposer un gdt dédié à ce sujet lors de la prochaine réunion mensuelle


   * host.server.distribution
       * afin de répondre au besoin de détection de Yunohost
       * ~~host.distribution~~ vs host.server.distribution
           * nous avons déjà host.server.type
       * Nom générique de la distribution installée sur le serveur (type STRING, obligatoire, ex. YunoHost)
       * host.server.distribution = 
       * TODO rajouter à tous les modèles de service et hosting Cpm FAIT
       * TODO rajouter la règle de vérification dans StatoolInfos Cpm FAIT
       * TODO rajouter le graphic dans StatoolInfos Cpm FAIT


   * service.registration
       * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/service.properties#L67](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/service.properties#L67)
       * Free : inscription nécessaire mais ouverte à tout le monde et gratuite.
       * lors d'un atelier, des participants ont été troublés par « nécessaire »
       * le retirer ou le garder ?


   * service.registration.load :
       * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/service.properties#L72](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/service.properties#L72)
       * lors d'un atelier, des participants ont été troublés par la valeur « FREE »
       * idée ?


   * ajout de organization.type ?
       * ASSOCIATION
       * COLLECTIVE
       * COOPERATIVE
       * MICROENTERPRISE
       * ENTERPRISE
       * INDIVIDUAL
       * OTHER


   * fiche properties hébergement
       * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/hosting.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/hosting.properties)
           * la question se pose également pour la section host normale
               * d'ailleurs le cas vps.* est traitable dans host.*
               * recenser les systèmes d'exploitation des vm et des hosts ?
                   * host.os
               * recenser les systèmes d'hypervision des vm et des hosts ?
                   * host.hypervisor
               * service.os // hosting.os : compliqué à exprimer / ne correspond pas vraiment
           * on collecte déjà beaucoup d'informations,  est-ce bien nécessaire ?
               * permettrait de faire se questionner sur le logiciel de supervision utilisé
       * projection d'intégration dans stats.chatons.org
           * besoin d'un bouton « Services dédiés » 
           * certainement renommer « Services » en « Services mutualisés » +1
           * page d'index : 
               * ajouter un nombre spécifique pour les services dédiés
               * ajouter un indicateur pour différencier
               * dans le tableau : un colonne pour les services dédiés / une pour les services mutualisés
           * page d'un membre :
               * ajouter un nombre spécifique pour les services dédiés
               * ajouter un indicateur pour différencier
           * nommage : service mutualisé vs service dédié
           * faire des essais
               * TODO Angie FAIT : créer une fiche "fausse" fiche hosting.properties pour Framasoft afin de tester
                   * [https://framagit.org/framasoft/chatons/-/blob/main/Fichiers\_properties/hosting-nextcloud.properties](https://framagit.org/framasoft/chatons/-/blob/main/Fichiers\_properties/hosting-nextcloud.properties)
               * TODO Angie : demander à ljf de compléter une fiche hosting.properties pour l'une des offres d'hébergement qu'il propose
           * déjà 2 fichiers properties remplis !




## 62e réunion du groupe de travail

**jeudi 05 mai 2022 à 11h15**

Personnes présentes : Cpm, Jérémy (mais en coup de vent)


### Revue de [https://stats.chatons.org/](https://stats.chatons.org/) 

   * page générique d'un chaton : 
       * penser à augmenter le code html avec les informations de properties pour faciliter le futur réagencement  UI/UX
           * TODO Cpm
   * page « Statistiques » (fédération) :
       * ajouter un donuts sur les services de paiement
           * TODO Cpm 
   * un jour peut-être :
       * pouvoir cliquer sur les graphiques pour voir la liste de résultats correspondant
           * par exemple pour les types d'inscription (à un service)
       * donuts sur les pays
           * pouvoir cliquer sur les résultats du camembert pour avoir une liste des chatons par pays
   * pages Uptimes (Federation, Organization, Services)
       * des améliorations à faire
           * TODO Cpm visibilité autres liens
           * TODO mrflos (pour l'été) : bidouiller la page statsuptime pour utiliser les filtres par état en js datatables
       * questions de statut manuel vs statut mesuré (page organization)
           * statut manuel seulement
           * statut mesuré seulement
           * les deux
           * un seul combiné des deux
           * discussion :
               * est-ce que la version manuelle est encore utile ? pertinence du mesuré
               * se poser la question de ce que cherche l'utilisateur
               * cas des statuts manuels « en travaux » ou  « fermé »
               * le statut manuel est plus important que le statut mesuré, respecté l'expression des admins
               * ne surtout pas afficher les deux
               * étudier la conjonction
           * TODO Cpm voir pour une version « combinée » avec bulle informative
   * Flo :
       * peut être avoir dans résumé des moyennes sans graphes, genre du texte « sur 2020 XXX visiteurs uniques, YYY ips différentes »
           * Cpm : une notion de « tendance » ?
           * Cpm : donner exemple ?
           * TODO Flo, à réfléchir l'enrichissement de texte des graphes toujours en TODO  > GT le 20 juillet > décalé
           * TODO Flo, à réfléchir à des cadres de tendances dans « Résumé » toujours en  > GT le 20 juillet > décalé
       * changer les intitulés « Web » et « Spécifique » par « Graphes de visites web » ou plus court « Graphes Web » et « Statistiques propres aux services » ou plus court « Stats des services » ?
           * Cpm : préciser l'intention
           * Flo : expliquer les items du menu type > GT le 20 juillet (annulé cause covid...)  > décalé
           * TODO Cpm : ajouter des bulles
           * TODO Flo : tester le menu métriques auprès de personnes
               * en cours toujours en TODO
           * TODO réfléchir
       * TODO Cpm afficher les champs nom et description des métrics dans les diagrammes
   * site statique vs site dynamique ?
       * différence de besoin entre Libre-service.eu (métrique au jour) et le collectif CHATONS (mérique au mois)
       * supprimer les vues années, semaines et jours ?
       * supprimer les périodes 12 mois, 2020 et 2021 ?
       * site statique :
           * avantages : simplicité de déploiement (est-ce nécessaire ?)
           * inconvénients : prend de la place sur disque
       * pour site dynamique :
           * avantages : plus grande interactivité, plus de liberté fonctionnelles
           * inconvénients : nécessite l'installation d'un serveur d'application Java, du travail pour finaliser
       * avis :
           * Antoine : pas de problème au site dynamique
           * Flo : peut-être finir les fonctionnalités en cours avant de faire une version 2 dynamique avec des optimisations
   * Test disponibilité en ipv6 :
       * [https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232](https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232)
       * est-ce une information déclarative ou à calculer ?
           * mieux si c'est calculé car plus à jour et un truc en moins à faire pour les admins
       * sujet non urgent, à approfondir
       * continuer à évaluer les besoins
       * page ipv6 pour avoir un aperçu du point de vue du collectif
   * Vérifier la présence de trackers basic. 
       * [https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232](https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232)
       * oui, très pertinent
       * valorisation : à envisager page dédiée
   * valeurs fausses du metrics visiteurs de pyStatoolInfos (utilisé par Sleto et Framasoft)
       * en mars : 140M de visiteurs pour 7M de visites
       * TODO Contacter Sleto : CPM FAITx2
       * TODO attendre retour
       * TODO Angie : changer de générateur ? autre ?


### Revue générateur de fichiers métrics (statoolinfos)

   * ajout support Jitsi :
       * TODO Cpm en cours
   * ajout support Nextcloud
   * ajout support XMPP
   * metrics http
       * améliorer détection OS TODO Cpm
       * améliorer détection date TODO Cpm
   * pour info: mrflos travaille sur scrumblr et pourra faire la page stat
       * \o/
   * todo mrflos : contacter tobias a propos de l'appli yunohost chatons FAIT le 21 avril
   * TODO attendre réponse tobias


### Revue des catégories

   *  ([https://stats.chatons.org/category-autres.xhtml)](https://stats.chatons.org/category-autres.xhtml)) :
   * nouvelle catégorie Proxy
       * origine du besoin : PeerJS Server
       * autres services similaires
           * Squid, Nitter, Invidious, bibliogram
       * nommage de la catégorie : proxy  (présent dans le Larousse)
       * TODO Angie création de la catégorie "Proxy" FAIT
       * TODO Angie : ajouter un picto
       * TODO répondre forum
   * Open edX~~ \& Weblate~~
       * [https://forum.chatons.org/t/ajout-de-open-edx-weblate-sur-chatons-org/3417](https://forum.chatons.org/t/ajout-de-open-edx-weblate-sur-chatons-org/3417)
       * autres logiciels à y ranger : Moodle, Scenarii, YesWiki
       * nommage : plateforme de formation (LMS)
       * TODO Angie enrichir le fichier categories.properties FAIT
       * TODO Angie : ajouter un picto lms.svg
       * TODO répondre dans le forum
   * ~~Open edX \& ~~Weblate
       * [https://forum.chatons.org/t/ajout-de-open-edx-weblate-sur-chatons-org/3417](https://forum.chatons.org/t/ajout-de-open-edx-weblate-sur-chatons-org/3417)
       * outil collaboratif de traduction de logiciels ou de sites webs
       * nommage de la catégorie : outils de traductions
       * chemin : translator
       * TOD Angie enrichir le fichier categories.properties FAIT
       * TODO Angie : ajouter un picto translator.svg
       * TODO répondre dans le forum 


### Revue des fichiers properties de membres

   * passer en revue :
       *  [https://stats.chatons.org/chatons-crawl.xhtml](https://stats.chatons.org/chatons-crawl.xhtml)
           * plus tard
       * [https://stats.chatons.org/chatons-propertyalerts.xhtml](https://stats.chatons.org/chatons-propertyalerts.xhtml)
           * 498 ~~284~~ ~~289 ~~ ~~292~~  ~~316~~ rouges, 1209 ~~1220~~  ~~1217~~  ~~1220~~  ~~1226~~ jaunes
           * communiquer un jour prochain (au moment de la prochaine release - celle avec les metrics)
   * rapprochement avec fiches chatons.org (prospective) :
       * constat qu'il y a eu une convergence de la fiche service sur chatons.org et le fichier properties de service
       * comment pourrait s'envisager une étape supplémentaire de rapprochement ?
       * niveau organisation :
           * si on complète le formulaire de la fiche structure (?) sur chatons.org alors chatons.org serait à même de générer automatiquement le fichier properties de l'organisation
               * Si on automatise à la création d'une fiche, on va se retrouver avec les fiches des candidats chatons et autres huluberlus qui créent des fiches sans jamais candidater. Il faudrait donc que la génération n'intervienne que lorsqu'on modifie la valeur du champ `Etat de la candidature` en `chaton approuvé`    +1 +1
           * avantages :
               * un plus grand nombre d'organisations suivies dans stats.chatons.org
               * une gestion automatique
           * inconvénients :
               * besoin d'étudier des cas (suppression de fiche structure, sortie du collectif, etc.)
       *  niveau service :
           * si on complète le formulaire de la fiche service sur chatons.org alors chatons.org serait à même de générer automatiquement le fichier properties de service
               * comment la génération automatique de ces fiches service.properties viendront-elles alimenter la section Subs de fiches organization.properties ?
                   * très important oui
           * avantages :
               *  un plus grand nombre de services dans stats.chatons.org
               * une gestion automatique
           * besoin d'étudier des cas (suppression de fiche service, sortie du collectif, etc.)
           * Premier pas :
               * Rassembler les infos :
                   * Le nom de l'information et où se trouve-t-elle (si ça se trouve ? et où : fiche Chatons ? Properties) ?
       * TODO recensement comparatif + table de concordance, Angie \o/


### Revue des tickets

   * [https://framagit.org/chatons/chatonsinfos/-/issues/1](https://framagit.org/chatons/chatonsinfos/-/issues/1)
       * Redesign des encarts au dessus des tableaux
       * prévu lorsqu'on aura toutes les informations affichées
       * statut : plus tard
   * [https://framagit.org/chatons/chatonsinfos/-/issues/2](https://framagit.org/chatons/chatonsinfos/-/issues/2)
       * Dans le tableau des services de la fiche organisation des chatons, supprimer la colonne "Organisation"
       * Cpm : ce tableau est une vue mutualisée entre plusieurs pages : organisation, services, catégorie,  logiciel ; l'information est effectivement redondante pour la page organisation, mais ça permet de conserver l'homogénéité de la vue.
       * Cpm : pour gagner de la place, possibilité de ne mettre que le logo de l'organisation et le nom en bulle
       * statut : réfléchir et sinon sera traité par la grande revue visuelle prévue un jour
   * [https://framagit.org/chatons/chatonsinfos/-/issues/4](https://framagit.org/chatons/chatonsinfos/-/issues/4)
       * Penser un nouveau fichier properties dédié aux offres non logicielles, celle d'hébergement
       * statut : priorité aux services utilisateurs donc pertinent mais plus tard


### Revue des merge requests

   * [https://framagit.org/framasoft/chatons/-/merge\_requests](https://framagit.org/framasoft/chatons/-/merge\_requests)


### Revue du forum

   * [https://forum.chatons.org/c/collectif/stats-chatons-org/83](https://forum.chatons.org/c/collectif/stats-chatons-org/83)
   * RAS


### Revue des disponibilités des services (uptimes)

   * [https://stats.chatons.org/chatons-uptimes.xhtml](https://stats.chatons.org/chatons-uptimes.xhtml)
   * ~~Salons JabberFR [admins@jabberfr.org]~~
       * ~~intermittent, à surveiller pour la prochaine fois (mais c'est plutôt leur site web que le service lui-même)~~
       * ~~réponse que ça vient du matériel, que faire ?~~
       * depuis 2 semains, sont verts, donc on va dire que c'est bon
   * ValiseChaprilOrg
       * de pire en pire (4 jours rouges)
       * post mastodon pb identifié maintenance en cours
   * Colibri > Visio
       * toujours rouge
       * en cours de redéploiement
   * Sleto > Equipe
       * TODO Jeey/MrFlos contacter
           * Jeey relance (05/05) - FAIT (05/05)
   * SiickServices > Siickurl
       * TODO Jeey contacter
           * Jeey relance (05/05) - FAIT (05/05)
   * Zici
       * tous les services HS (zici.fr ne répond plus)
       * TODO Jérémy le contacte - FAIT (05/05)
   * Devloprog
       * systèmes bagottent
       * TODO Jérémy contacte - FAIT (05/05)


### Revue de l'ontologie

   * rappels :
       * l'ordre des questions à se poser : préfixe, sous-préfixe
       * on rajout .count si le mot signifiant est le préfixe
       * métriques HTTP :
           * contexte :
               * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties)
               * [http://www.webalizer.org/webalizer\_help.html](http://www.webalizer.org/webalizer\_help.html)
                   * [https://web.archive.org/web/20210330151216/http://www.webalizer.org/webalizer\_help.html](https://web.archive.org/web/20210330151216/http://www.webalizer.org/webalizer\_help.html)


   * mesure du CO2 :
       * proposer un gdt dédié à ce sujet lors de la prochaine réunion mensuelle


   * host.server.distribution
       * afin de répondre au besoin de détection de Yunohost
       * ~~host.distribution~~ vs host.server.distribution
           * nous avons déjà host.server.type
       * Nom générique de la distribution installée sur le serveur (type STRING, obligatoire, ex. YunoHost)
       * host.server.distribution = 
       * TODO rajouter à tous les modèles de service et hosting Cpm FAIT
       * TODO rajouter la règle de vérification dans StatoolInfos Cpm FAIT
       * TODO rajouter le graphic dans StatoolInfos Cpm FAIT


   * service.registration
       * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/service.properties#L67](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/service.properties#L67)
       * Free : inscription nécessaire mais ouverte à tout le monde et gratuite.
       * lors d'un atelier, des participants ont été troublés par « nécessaire »
       * le retirer ou le garder ?


   * service.registration.load :
       * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/service.properties#L72](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/service.properties#L72)
       * lors d'un atelier, des participants ont été troublés par la valeur « FREE »
       * idée ?


   * ajout de organization.type ?
       * ASSOCIATION
       * COLLECTIVE
       * COOPERATIVE
       * MICROENTERPRISE
       * ENTERPRISE
       * INDIVIDUAL
       * OTHER


   * fiche properties hébergement
       * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/hosting.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/hosting.properties)
           * la question se pose également pour la section host normale
               * d'ailleurs le cas vps.* est traitable dans host.*
               * recenser les systèmes d'exploitation des vm et des hosts ?
                   * host.os
               * recenser les systèmes d'hypervision des vm et des hosts ?
                   * host.hypervisor
               * service.os // hosting.os : compliqué à exprimer / ne correspond pas vraiment
           * on collecte déjà beaucoup d'informations,  est-ce bien nécessaire ?
               * permettrait de faire se questionner sur le logiciel de supervision utilisé
       * projection d'intégration dans stats.chatons.org
           * besoin d'un bouton « Services dédiés » 
           * certainement renommer « Services » en « Services mutualisés » +1
           * page d'index : 
               * ajouter un nombre spécifique pour les services dédiés
               * ajouter un indicateur pour différencier
               * dans le tableau : un colonne pour les services dédiés / une pour les services mutualisés
           * page d'un membre :
               * ajouter un nombre spécifique pour les services dédiés
               * ajouter un indicateur pour différencier
           * nommage : service mutualisé vs service dédié
           * faire des essais
               * TODO Angie FAIT : créer une fiche "fausse" fiche hosting.properties pour Framasoft afin de tester
                   * [https://framagit.org/framasoft/chatons/-/blob/main/Fichiers\_properties/hosting-nextcloud.properties](https://framagit.org/framasoft/chatons/-/blob/main/Fichiers\_properties/hosting-nextcloud.properties)
               * TODO Angie : demander à ljf de compléter une fiche hosting.properties pour l'une des offres d'hébergement qu'il propose
           * déjà 2 fichiers properties remplis !


## 63e réunion du groupe de travail

**jeudi 19 mai 2022 à 11h15**

Personnes présentes : Angie / MrFlos / Cpm


### Revue de [https://stats.chatons.org/](https://stats.chatons.org/) 

   * page générique d'un chaton : 
       * penser à augmenter le code html avec les informations de properties pour faciliter le futur réagencement  UI/UX
           * TODO Cpm
   * page « Statistiques » (fédération) :
       * ajouter un donuts sur les services de paiement
           * TODO Cpm 
   * un jour peut-être :
       * pouvoir cliquer sur les graphiques pour voir la liste de résultats correspondant
           * par exemple pour les types d'inscription (à un service)
       * donuts sur les pays
           * pouvoir cliquer sur les résultats du camembert pour avoir une liste des chatons par pays
   * pages Uptimes (Federation, Organization, Services)
       * des améliorations à faire
           * TODO Cpm visibilité autres liens
           * TODO mrflos (pour l'été) : bidouiller la page statsuptime pour utiliser les filtres par état en js datatables
       * questions de statut manuel vs statut mesuré (page organization)
           * statut manuel seulement
           * statut mesuré seulement
           * les deux
           * un seul combiné des deux
           * discussion :
               * est-ce que la version manuelle est encore utile ? pertinence du mesuré
               * se poser la question de ce que cherche l'utilisateur
               * cas des statuts manuels « en travaux » ou  « fermé »
               * le statut manuel est plus important que le statut mesuré, respecté l'expression des admins
               * ne surtout pas afficher les deux
               * étudier la conjonction
           * TODO Cpm voir pour une version « combinée » avec bulle informative
   * Flo :
       * peut être avoir dans résumé des moyennes sans graphes, genre du texte « sur 2020 XXX visiteurs uniques, YYY ips différentes »
           * Cpm : une notion de « tendance » ?
           * Cpm : donner exemple ?
           * TODO Flo, à réfléchir l'enrichissement de texte des graphes toujours en TODO  > GT le 20 juillet > décalé
           * TODO Flo, à réfléchir à des cadres de tendances dans « Résumé » toujours en  > GT le 20 juillet > décalé
       * changer les intitulés « Web » et « Spécifique » par « Graphes de visites web » ou plus court « Graphes Web » et « Statistiques propres aux services » ou plus court « Stats des services » ?
           * Cpm : préciser l'intention
           * Flo : expliquer les items du menu type > GT le 20 juillet (annulé cause covid...)  > décalé
           * TODO Cpm : ajouter des bulles
           * TODO Flo : tester le menu métriques auprès de personnes
               * en cours toujours en TODO
           * TODO réfléchir
       * TODO Cpm afficher les champs nom et description des métrics dans les diagrammes
   * site statique vs site dynamique ?
       * différence de besoin entre Libre-service.eu (métrique au jour) et le collectif CHATONS (mérique au mois)
       * supprimer les vues années, semaines et jours ?
       * supprimer les périodes 12 mois, 2020 et 2021 ?
       * site statique :
           * avantages : simplicité de déploiement (est-ce nécessaire ?)
           * inconvénients : prend de la place sur disque
       * pour site dynamique :
           * avantages : plus grande interactivité, plus de liberté fonctionnelles
           * inconvénients : nécessite l'installation d'un serveur d'application Java, du travail pour finaliser
       * avis :
           * Antoine : pas de problème au site dynamique
           * Flo : peut-être finir les fonctionnalités en cours avant de faire une version 2 dynamique avec des optimisations
   * Test disponibilité en ipv6 :
       * [https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232](https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232)
       * est-ce une information déclarative ou à calculer ?
           * mieux si c'est calculé car plus à jour et un truc en moins à faire pour les admins
       * sujet non urgent, à approfondir
       * continuer à évaluer les besoins
       * page ipv6 pour avoir un aperçu du point de vue du collectif
   * Vérifier la présence de trackers basic. 
       * [https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232](https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232)
       * oui, très pertinent
       * valorisation : à envisager page dédiée
   * valeurs fausses du metrics visiteurs de pyStatoolInfos (utilisé par Sleto et Framasoft)
       * en mars : 140M de visiteurs pour 7M de visites
       * TODO Contacter Sleto : CPM FAITx2
       * TODO attendre retour
       * TODO Angie : changer de générateur ? autre ?
           * URLs fichiers métrics désactivées
           * ok pour export Matomo 


### Revue générateur de fichiers métrics (statoolinfos)

   * ajout support Jitsi :
       * TODO Cpm en cours
   * ajout support Nextcloud
   * ajout support XMPP
   * metrics http
       * améliorer détection OS TODO Cpm
       * améliorer détection date TODO Cpm
   * pour info: mrflos travaille sur scrumblr et pourra faire la page stat
       * \o/
   * todo mrflos : contacter tobias a propos de l'appli yunohost chatons FAIT le 21 avril
       * ping (19/05/2022) FAIT 
       * TODO attendre réponse tobias


### Revue des catégories

   *  ([https://stats.chatons.org/category-autres.xhtml)](https://stats.chatons.org/category-autres.xhtml)) :
   * nouvelle catégorie Proxy
       * origine du besoin : PeerJS Server
       * autres services similaires
           * Squid, Nitter, Invidious, bibliogram
       * nommage de la catégorie : proxy  (présent dans le Larousse)
       * TODO Angie création de la catégorie "Proxy" FAIT
       * TODO Angie : ajouter un picto FAIT
       * ~~TODO répondre forum~~
   * Open edX~~ \& Weblate~~
       * [https://forum.chatons.org/t/ajout-de-open-edx-weblate-sur-chatons-org/3417](https://forum.chatons.org/t/ajout-de-open-edx-weblate-sur-chatons-org/3417)
       * autres logiciels à y ranger : Moodle, Scenarii, YesWiki
       * nommage : plateforme de formation (LMS)
       * TODO Angie enrichir le fichier categories.properties FAIT
       * TODO Angie : ajouter un picto lms.svg FAIT
       * ~~TODO répondre dans le forum~~
   * ~~Open edX \& ~~Weblate
       * [https://forum.chatons.org/t/ajout-de-open-edx-weblate-sur-chatons-org/3417](https://forum.chatons.org/t/ajout-de-open-edx-weblate-sur-chatons-org/3417)
       * outil collaboratif de traduction de logiciels ou de sites webs
       * nommage de la catégorie : outils de traductions
       * chemin : translator
       * TOD Angie enrichir le fichier categories.properties FAIT
       * TODO Angie : ajouter un picto translator.svg FAIT
       * ~~TODO répondre dans le forum ~~


### Revue des fichiers properties de membres

   * passer en revue :
       *  [https://stats.chatons.org/chatons-crawl.xhtml](https://stats.chatons.org/chatons-crawl.xhtml)
           * plus tard
       * [https://stats.chatons.org/chatons-propertyalerts.xhtml](https://stats.chatons.org/chatons-propertyalerts.xhtml)
           * 498 ~~284~~ ~~289 ~~ ~~292~~  ~~316~~ rouges, 1209 ~~1220~~  ~~1217~~  ~~1220~~  ~~1226~~ jaunes
           * communiquer un jour prochain (au moment de la prochaine release - celle avec les metrics)
   * rapprochement avec fiches chatons.org (prospective) :
       * constat qu'il y a eu une convergence de la fiche service sur chatons.org et le fichier properties de service
       * comment pourrait s'envisager une étape supplémentaire de rapprochement ?
       * niveau organisation :
           * si on complète le formulaire de la fiche structure (?) sur chatons.org alors chatons.org serait à même de générer automatiquement le fichier properties de l'organisation
               * Si on automatise à la création d'une fiche, on va se retrouver avec les fiches des candidats chatons et autres huluberlus qui créent des fiches sans jamais candidater. Il faudrait donc que la génération n'intervienne que lorsqu'on modifie la valeur du champ `Etat de la candidature` en `chaton approuvé`    +1 +1
           * avantages :
               * un plus grand nombre d'organisations suivies dans stats.chatons.org
               * une gestion automatique
           * inconvénients :
               * besoin d'étudier des cas (suppression de fiche structure, sortie du collectif, etc.)
       *  niveau service :
           * si on complète le formulaire de la fiche service sur chatons.org alors chatons.org serait à même de générer automatiquement le fichier properties de service
               * comment la génération automatique de ces fiches service.properties viendront-elles alimenter la section Subs de fiches organization.properties ?
                   * très important oui
           * avantages :
               *  un plus grand nombre de services dans stats.chatons.org
               * une gestion automatique
           * besoin d'étudier des cas (suppression de fiche service, sortie du collectif, etc.)
           * Premier pas :
               * Rassembler les infos :
                   * Le nom de l'information et où se trouve-t-elle (si ça se trouve ? et où : fiche Chatons ? Properties) ?
       * TODO recensement comparatif + table de concordance, Angie \o/


### Revue des tickets

   * [https://framagit.org/chatons/chatonsinfos/-/issues/1](https://framagit.org/chatons/chatonsinfos/-/issues/1)
       * Redesign des encarts au dessus des tableaux
       * prévu lorsqu'on aura toutes les informations affichées
       * statut : plus tard
   * [https://framagit.org/chatons/chatonsinfos/-/issues/2](https://framagit.org/chatons/chatonsinfos/-/issues/2)
       * Dans le tableau des services de la fiche organisation des chatons, supprimer la colonne "Organisation"
       * Cpm : ce tableau est une vue mutualisée entre plusieurs pages : organisation, services, catégorie,  logiciel ; l'information est effectivement redondante pour la page organisation, mais ça permet de conserver l'homogénéité de la vue.
       * Cpm : pour gagner de la place, possibilité de ne mettre que le logo de l'organisation et le nom en bulle
       * statut : réfléchir et sinon sera traité par la grande revue visuelle prévue un jour
   * [https://framagit.org/chatons/chatonsinfos/-/issues/4](https://framagit.org/chatons/chatonsinfos/-/issues/4)
       * Penser un nouveau fichier properties dédié aux offres non logicielles, celle d'hébergement
       * statut : priorité aux services utilisateurs donc pertinent mais plus tard


##" Revue des merge requests

   * [https://framagit.org/framasoft/chatons/-/merge\_requests](https://framagit.org/framasoft/chatons/-/merge\_requests)


### Revue du forum

   * [https://forum.chatons.org/c/collectif/stats-chatons-org/83](https://forum.chatons.org/c/collectif/stats-chatons-org/83)
   * RAS


### Revue des disponibilités des services (uptimes)

   * [https://stats.chatons.org/chatons-uptimes.xhtml](https://stats.chatons.org/chatons-uptimes.xhtml)
   * ValiseChaprilOrg
       * de pire en pire (4 jours rouges)
       * post mastodon pb identifié maintenance en cours
   * Colibri > Visio
       * toujours rouge
       * en cours de redéploiement
       * retour au vert
       * FAIT
   * Sleto > Equipe
       * rouge depuis au moins 3 semaines
       * TODO Jeey/MrFlos contacter
           * Jeey relance (05/05)
   * SiickServices > Siickurl
       * TODO Jeey contacter
           * Jeey relance (05/05)
           * Le service est coupé pour l'instant car en refonte afin de mettre un login utilisateur afin de l'utiliser car le service était trop utilisé par des personnes malhonnêtes afin de rediriger vers des liens phishing. (05/05)
           * ok pour l'info
           * l'URL du fichier properties de service a été supprimé de son fichier properties  organisation, dommage, recommandation de la laisser et de passer le service à DOWN
           * TODO Cpm trouver à commuquer pour procédure retrait d'un service
   * Zici
       * tous les services HS (zici.fr ne répond plus)
       * TODO Jérémy le contacte
       * Effectivement on est down sur une partie des services web (mail      et cloud ok) chez Zici \& Retzien  depuis hier après-midi.      C'est identifié et en cours de résolution (on attends globalement      la propagation DNS : [https://status.zici.fr/incidents/33](https://status.zici.fr/incidents/33) ) (05/05)
       * retour au vert
       * FAIT
   * Devloprog
       * ne semble pas hyper bloqué mais ça serait bien de résoudre
       * systèmes bagottent
       * TODO Jérémy contacte


### Revue de l'ontologie

   * rappels :
       * l'ordre des questions à se poser : préfixe, sous-préfixe
       * on rajout .count si le mot signifiant est le préfixe
       * métriques HTTP :
           * contexte :
               * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties)
               * [http://www.webalizer.org/webalizer\_help.html](http://www.webalizer.org/webalizer\_help.html)
                   * [https://web.archive.org/web/20210330151216/http://www.webalizer.org/webalizer\_help.html](https://web.archive.org/web/20210330151216/http://www.webalizer.org/webalizer\_help.html)


   * mesure du CO2 :
       * proposer un gdt dédié à ce sujet lors de la prochaine réunion mensuelle


   * host.server.distribution
       * pas très propagé
       * TODO Cpm faire un rappel forum (faire un nouveau post)


   * service.registration
       * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/service.properties#L67](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/service.properties#L67)
       * Free : inscription nécessaire mais ouverte à tout le monde et gratuite.
       * lors d'un atelier, des participants ont été troublés par « nécessaire »
       * le retirer ou le garder ?
       * a priori le garder mais il faut lever l'ambiguité du service auquel on s'inscrit pour accéder à des fonctions de "création" et le fait de pouvoir répondre à ce qui a été créé (ex: Yakforms)
           * utilisateurs : bénéficiaires (sans inscription) + créateurs (avec inscription)
           * organisateur/~~animateur~~/créateur de contenu + participant
           * rajouter une entrée NONE+FREE ou créer un champ distinct


   * service.registration.load :
       * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/service.properties#L72](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/service.properties#L72)
       * lors d'un atelier, des participants ont été troublés par la valeur « FULL »
       * pas convaincu donc on laisse
       * FAIT


   * ajout de organization.type ?
       * valeur de fiche chatons :
           * ASSOCIATION # Association loi 1901
           * ~~COLLECTIVE~~ vs ~~INFORMALCOLLECTIVE ~~vs INFORMAL # Collectif informel
           * ~~COOPERATIVE COMPANY ~~vs COOPERATIVE
               * COOPERATIVE : entreprise coopérative 
           * ~~TINYCOMPANY~~ vs MICROCOMPANY # Entreprise individuelle (exemples: auto-entreprise, EU…)
           * COMPANY # Entreprise
           * INDIVIDUAL # Individuel
           * OTHER # Autre type d'organisation
       * ligne pour le modèles :
    # Type d'organisation, un parmi {qsdfqsfdqsdf}, obligatoire).

       * homogénéiser : OUI
       * TODO Angie rajouter modèle + changelog
       * TODO Cpm coder fichier properties
       * TODO Cpm changelog
       * TODO Cpm annonce forum


   * fiche properties hébergement
       * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/hosting.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/hosting.properties)
       * projection d'intégration dans stats.chatons.org
           * besoin d'un bouton « Services dédiés » 
           * certainement renommer « Services » en « Services mutualisés » +1
           * page d'index : 
               * ajouter un nombre spécifique pour les services dédiés
               * ajouter un indicateur pour différencier
               * dans le tableau : un colonne pour les services dédiés / une pour les services mutualisés
           * page d'un membre :
               * ajouter un nombre spécifique pour les services dédiés
               * ajouter un indicateur pour différencier
           * nommage : service mutualisé vs service dédié
           * faire des essais
               * TODO Angie FAIT : créer une fiche "fausse" fiche hosting.properties pour Framasoft afin de tester
                   * [https://framagit.org/framasoft/chatons/-/blob/main/Fichiers\_properties/hosting-nextcloud.properties](https://framagit.org/framasoft/chatons/-/blob/main/Fichiers\_properties/hosting-nextcloud.properties)
           * déjà 2 fichiers properties remplis !


   * hosts.os + host.hypervisor :
           * la question se pose également pour la section host normale
               * d'ailleurs le cas vps.* est traitable dans host.*
               * recenser les systèmes d'exploitation des vm et des hosts ?
                   * host.os
               * recenser les systèmes d'hypervision des vm et des hosts ?
                   * host.hypervisor
               * service.os // hosting.os : compliqué à exprimer / ne correspond pas vraiment
           * on collecte déjà beaucoup d'informations,  est-ce bien nécessaire ?
               * permettrait de faire se questionner sur le logiciel de supervision utilisé


## 64e réunion du groupe de travail

**jeudi 16 juin 2022 à 11h15**

Personnes présentes : Cpm / Angie / Jeey


### Revue de [https://stats.chatons.org/](https://stats.chatons.org/) 

   * page générique d'un chaton : 
       * penser à augmenter le code html avec les informations de properties pour faciliter le futur réagencement  UI/UX
           * TODO Cpm
   * page « Statistiques » (fédération) :
       * ajouter un donuts sur les services de paiement
           * TODO Cpm 
   * un jour peut-être :
       * pouvoir cliquer sur les graphiques pour voir la liste de résultats correspondant
           * par exemple pour les types d'inscription (à un service)
       * donuts sur les pays
           * pouvoir cliquer sur les résultats du camembert pour avoir une liste des chatons par pays
   * pages Uptimes (Federation, Organization, Services)
       * des améliorations à faire
           * TODO Cpm visibilité autres liens
           * TODO mrflos (pour l'été) : bidouiller la page statsuptime pour utiliser les filtres par état en js datatables
       * questions de statut manuel vs statut mesuré (page organization)
           * statut manuel seulement
           * statut mesuré seulement
           * les deux
           * un seul combiné des deux
           * discussion :
               * est-ce que la version manuelle est encore utile ? pertinence du mesuré
               * se poser la question de ce que cherche l'utilisateur
               * cas des statuts manuels « en travaux » ou  « fermé »
               * le statut manuel est plus important que le statut mesuré, respecté l'expression des admins
               * ne surtout pas afficher les deux
               * étudier la conjonction
           * TODO Cpm voir pour une version « combinée » avec bulle informative
   * Flo :
       * peut être avoir dans résumé des moyennes sans graphes, genre du texte « sur 2020 XXX visiteurs uniques, YYY ips différentes »
           * Cpm : une notion de « tendance » ?
           * Cpm : donner exemple ?
           * TODO Flo, à réfléchir l'enrichissement de texte des graphes toujours en TODO  > GT le 20 juillet > décalé
           * TODO Flo, à réfléchir à des cadres de tendances dans « Résumé » toujours en  > GT le 20 juillet > décalé
       * changer les intitulés « Web » et « Spécifique » par « Graphes de visites web » ou plus court « Graphes Web » et « Statistiques propres aux services » ou plus court « Stats des services » ?
           * Cpm : préciser l'intention
           * Flo : expliquer les items du menu type > GT le 20 juillet (annulé cause covid...)  > décalé
           * TODO Cpm : ajouter des bulles
           * TODO Flo : tester le menu métriques auprès de personnes
               * en cours toujours en TODO
           * TODO réfléchir
       * TODO Cpm afficher les champs nom et description des métrics dans les diagrammes
   * site statique vs site dynamique ?
       * différence de besoin entre Libre-service.eu (métrique au jour) et le collectif CHATONS (mérique au mois)
       * supprimer les vues années, semaines et jours ?
       * supprimer les périodes 12 mois, 2020 et 2021 ?
       * site statique :
           * avantages : simplicité de déploiement (est-ce nécessaire ?)
           * inconvénients : prend de la place sur disque
       * pour site dynamique :
           * avantages : plus grande interactivité, plus de liberté fonctionnelles
           * inconvénients : nécessite l'installation d'un serveur d'application Java, du travail pour finaliser
       * avis :
           * Antoine : pas de problème au site dynamique
           * Flo : peut-être finir les fonctionnalités en cours avant de faire une version 2 dynamique avec des optimisations
   * Test disponibilité en ipv6 :
       * [https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232](https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232)
       * est-ce une information déclarative ou à calculer ?
           * mieux si c'est calculé car plus à jour et un truc en moins à faire pour les admins
       * sujet non urgent, à approfondir
       * continuer à évaluer les besoins
       * page ipv6 pour avoir un aperçu du point de vue du collectif
   * Vérifier la présence de trackers basic. 
       * [https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232](https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232)
       * oui, très pertinent
       * valorisation : à envisager page dédiée


### Revue générateur de fichiers métrics (statoolinfos)

   * ajout support Jitsi :
       * TODO Cpm en cours
   * ajout support Nextcloud
   * ajout support XMPP
   * metrics http
       * améliorer détection OS TODO Cpm
       * améliorer détection date TODO Cpm
   * pour info: mrflos travaille sur scrumblr et pourra faire la page stat
       * \o/
   * todo mrflos : contacter tobias a propos de l'appli yunohost chatons FAIT le 21 avril
       * ping (19/05/2022) FAIT 
       * réponse tobias
           * Salut,
           * J’ai complètement oublié de te répondre, j’ai presque rien fait, j’ai juste un début de python :
               * #! /usr/bin/python3
               * import sys
               * sys.path.insert(0, « /usr/lib/moulinette/ »)
               * import yunohost
               * yunohost.init\_logging()
               * yunohost.init\_i18n()
               * from yunohost.app import app\_info()from yunohost.app import app\_list()
               * apps = [ app[‹ id ›] for app in app\_list()[‹ apps ›] ]
           * Si vous organiser un repo, je voudrais bien le lien, je pourrais essayer de passer du temps dessus.
       * Créer un dépôt -> Angie le créé > FAIT (16/06)
           * [https://framagit.org/chatons/chatonsinfos-yunohost](https://framagit.org/chatons/chatonsinfos-yunohost)
           * trouver un animateur de ce dépôt (créer, surveiller, répondre etc..) -> voir avecFlo, Tobias ou Aleks
           * TODO CPM faire un appel sur le forum


### Revue des catégories

   *  ([https://stats.chatons.org/category-autres.xhtml)](https://stats.chatons.org/category-autres.xhtml))
   * RAS




### Revue des fichiers properties de membres

   * passer en revue :
       *  [https://stats.chatons.org/chatons-crawl.xhtml](https://stats.chatons.org/chatons-crawl.xhtml)
           * plus tard
       * [https://stats.chatons.org/chatons-propertyalerts.xhtml](https://stats.chatons.org/chatons-propertyalerts.xhtml)
           * 500 ~~498~~ ~~284~~ ~~289 ~~ ~~292~~  ~~316~~ rouges, 1208 ~~1209~~ ~~1220~~  ~~1217~~  ~~1220~~  ~~1226~~ jaunes
           * communiquer un jour prochain (au moment de la prochaine release - celle avec les metrics)
   * rapprochement avec fiches chatons.org (prospective) :
       * constat qu'il y a eu une convergence de la fiche service sur chatons.org et le fichier properties de service
       * comment pourrait s'envisager une étape supplémentaire de rapprochement ?
       * niveau organisation :
           * si on complète le formulaire de la fiche structure (?) sur chatons.org alors chatons.org serait à même de générer automatiquement le fichier properties de l'organisation
               * Si on automatise à la création d'une fiche, on va se retrouver avec les fiches des candidats chatons et autres huluberlus qui créent des fiches sans jamais candidater. Il faudrait donc que la génération n'intervienne que lorsqu'on modifie la valeur du champ `Etat de la candidature` en `chaton approuvé`    +1 +1
           * avantages :
               * un plus grand nombre d'organisations suivies dans stats.chatons.org
               * une gestion automatique
           * inconvénients :
               * besoin d'étudier des cas (suppression de fiche structure, sortie du collectif, etc.)
       *  niveau service :
           * si on complète le formulaire de la fiche service sur chatons.org alors chatons.org serait à même de générer automatiquement le fichier properties de service
               * comment la génération automatique de ces fiches service.properties viendront-elles alimenter la section Subs de fiches organization.properties ?
                   * très important oui
           * avantages :
               *  un plus grand nombre de services dans stats.chatons.org
               * une gestion automatique
           * besoin d'étudier des cas (suppression de fiche service, sortie du collectif, etc.)
           * Premier pas :
               * Rassembler les infos :
                   * Le nom de l'information et où se trouve-t-elle (si ça se trouve ? et où : fiche Chatons ? Properties) ?
       * TODO recensement comparatif + table de concordance, Angie \o/



### Revue des tickets

   * [https://framagit.org/chatons/chatonsinfos/-/issues/1](https://framagit.org/chatons/chatonsinfos/-/issues/1)
       * Redesign des encarts au dessus des tableaux
       * prévu lorsqu'on aura toutes les informations affichées
       * statut : plus tard
   * [https://framagit.org/chatons/chatonsinfos/-/issues/2](https://framagit.org/chatons/chatonsinfos/-/issues/2)
       * Dans le tableau des services de la fiche organisation des chatons, supprimer la colonne "Organisation"
       * Cpm : ce tableau est une vue mutualisée entre plusieurs pages : organisation, services, catégorie,  logiciel ; l'information est effectivement redondante pour la page organisation, mais ça permet de conserver l'homogénéité de la vue.
       * Cpm : pour gagner de la place, possibilité de ne mettre que le logo de l'organisation et le nom en bulle
       * statut : réfléchir et sinon sera traité par la grande revue visuelle prévue un jour
   * [https://framagit.org/chatons/chatonsinfos/-/issues/4](https://framagit.org/chatons/chatonsinfos/-/issues/4)
       * Penser un nouveau fichier properties dédié aux offres non logicielles, celle d'hébergement
       * statut : priorité aux services utilisateurs donc pertinent mais plus tard


### Revue des merge requests

   * [https://framagit.org/framasoft/chatons/-/merge\_requests](https://framagit.org/framasoft/chatons/-/merge\_requests)


### Revue du forum

   * [https://forum.chatons.org/c/collectif/stats-chatons-org/83](https://forum.chatons.org/c/collectif/stats-chatons-org/83)
   * RAS


### Revue des disponibilités des services (uptimes)

   * [https://stats.chatons.org/chatons-uptimes.xhtml](https://stats.chatons.org/chatons-uptimes.xhtml)
   * ~~ValiseChaprilOrg~~
       * ~~de pire en pire (4 jours rouges)~~
       * ~~post mastodon pb identifié maintenance en cours~~
       * ~~retour au vert~~
   * Sleto > Equipe
       * rouge depuis au moins 3 semaines
       * TODO Jeey/MrFlos contacter
           * Jeey relance (05/05)
               * pas de réponse
       * TODO Angie relancer
   * Devloprog
       * ne semble pas hyper bloqué mais ça serait bien de résoudre
       * systèmes bagottent
       * TODO Jérémy contacte
       * (20/05) Reponse de Baptiste : 
           * merci pour ce message.
           * En effet, le problème est connu , mais je n'arrive pas vraiment à comprendre d'ou cela provient .
           * Une aide serait la bienvenue :) 
           * Je suppose que le problème provient de ma conf NGINX en reverese proxy  ...
               * [https://forum.chatons.org/t/uptime-ko-regulierement-mais-pas-partout/](https://forum.chatons.org/t/uptime-ko-regulierement-mais-pas-partout/)
   * Roflcopter > Cryptpad
       * le service est pourtant accessible
       * pb de certificat : curl: (60) SSL certificate problem: unable to get local issuer certificate
       * TODO Cpm : approfondir et contacter
   * Sans-nuage / ARN > Tâches
       * lenteur de réponse donc time out
       * TODO Jeey contacter (16/06)
           * FAIT (16/05)

### Revue de l'ontologie

   * rappels :
       * l'ordre des questions à se poser : préfixe, sous-préfixe
       * on rajout .count si le mot signifiant est le préfixe
       * métriques HTTP :
           * contexte :
               * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties)
               * [http://www.webalizer.org/webalizer\_help.html](http://www.webalizer.org/webalizer\_help.html)
                   * [https://web.archive.org/web/20210330151216/http://www.webalizer.org/webalizer\_help.html](https://web.archive.org/web/20210330151216/http://www.webalizer.org/webalizer\_help.html)


   * mesure du CO2 :
       * proposer un gdt dédié à ce sujet lors de la prochaine réunion mensuelle


   * host.server.distribution
       * pas très propagé
       * TODO Cpm voir le cas anancus
       * TODO Cpm faire un rappel forum (faire un nouveau post) ++
       * en profiter pour  :
           * service.registration.load
           * service.install.type
           * indiquer où allervoir


   * service.registration
       * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/service.properties#L67](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/service.properties#L67)
       * Free : inscription nécessaire mais ouverte à tout le monde et gratuite.
       * lors d'un atelier, des participants ont été troublés par « nécessaire »
       * le retirer ou le garder ?
       * a priori le garder mais il faut lever l'ambiguité du service auquel on s'inscrit pour accéder à des fonctions de "création" et le fait de pouvoir répondre à ce qui a été créé (ex: Yakforms)
           * utilisateurs : bénéficiaires (sans inscription) + créateurs (avec inscription)
           * organisateur/~~animateur~~/créateur de contenu + participant
           * rajouter une entrée NONE+FREE ou créer un champ distinct
       * champ très générique touchant à de nombreux concepts
       * décision de rester le plus générique possible
           * retrait du mot « nécessaire »
       * sujet clos


   * ajout de organization.type ?
       * valeur de fiche chatons :
           * ASSOCIATION # Association loi 1901
           * ~~COLLECTIVE~~ vs ~~INFORMALCOLLECTIVE ~~vs INFORMAL # Collectif informel
           * ~~COOPERATIVE COMPANY ~~vs COOPERATIVE
               * COOPERATIVE : entreprise coopérative 
           * ~~TINYCOMPANY~~ vs MICROCOMPANY # Entreprise individuelle (exemples: auto-entreprise, EU…)
           * COMPANY # Entreprise
           * INDIVIDUAL # Individuel
           * OTHER # Autre type d'organisation
       * ligne pour le modèles :
    # Type d'organisation, un parmi {qsdfqsfdqsdf}, obligatoire).

       * homogénéiser : OUI
       * TODO Angie rajouter modèle + changelog
       * TODO Cpm coder fichier properties
       * TODO Cpm changelog
       * TODO Cpm annonce forum


   * fiche properties hébergement
       * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/hosting.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/hosting.properties)
       * déjà 2 fichiers properties remplis !
       * projection d'intégration dans stats.chatons.org
           * besoin d'un bouton « Services dédiés » 
           * certainement renommer « Services » en « Services mutualisés » +1
           * page d'index : 
               * ajouter un nombre spécifique pour les services dédiés
               * ajouter un indicateur pour différencier
               * dans le tableau : un colonne pour les services dédiés / une pour les services mutualisés
           * page d'un membre :
               * ajouter un nombre spécifique pour les services dédiés
               * ajouter un indicateur pour différencier
           * nommage : service mutualisé vs service dédié
       * TODO communiquer auprès du collectif sur l'existence de cette nouvelle fiche
       * TODO intégrer dans le site stats.chatons.org


   * hosts.os + host.hypervisor :
       * la question se pose également pour la section host normale
           * d'ailleurs le cas vps.* est traitable dans host.*
           * recenser les systèmes d'exploitation des vm et des hosts ?
               * host.os
           * recenser les systèmes d'hypervision des vm et des hosts ?
               * host.hypervisor
           * service.os // hosting.os : compliqué à exprimer / ne correspond pas vraiment
       * on collecte déjà beaucoup d'informations,  est-ce bien nécessaire ?
           * permettrait de faire se questionner sur le logiciel de supervision utilisé
           * pas utile d'utiliser ce champ pour faire du contrôle
           * utile pour savoir voir quel logiciel est utilisé (solidarité)
       * puisque hosts.server.distribution existe alors hosts.os est moins utile
       * donc ajout :
           * *# Nom du logiciel hyperviseur (type STRING, optionnel, ex. KVM).*
           * host.provider.hypervisor = 
       * TODO implémenter STatoolInfos
       * TODO modifier CHANGELOG


   * prévoir de communiquer sur l'usage possible des stats


## 65e réunion du groupe de travail

**jeudi 21 juillet 2022 à 11h15**

Personnes présentes :

   * Cpm
   * Angie
   * Jeey


### Divers

   * atelier ChatonsInfo le lundi 25/07
       * [https://date.infini.fr/oUASJPfOlTsRTH9p](https://date.infini.fr/oUASJPfOlTsRTH9p)


### Revue de [https://stats.chatons.org/](https://stats.chatons.org/) 

   * page properties :
       * constat de lignes différentes entre enddate et endDate
       * TODO Cpm ne pas tenir compte de la casse
   * page générique d'un chaton : 
       * penser à augmenter le code html avec les informations de properties pour faciliter le futur réagencement  UI/UX
           * TODO Cpm
   * page « Statistiques » (fédération) :
       * ajouter un donuts sur les services de paiement
           * TODO Cpm 
   * un jour peut-être :
       * pouvoir cliquer sur les graphiques pour voir la liste de résultats correspondant
           * par exemple pour les types d'inscription (à un service)
       * donuts sur les pays
           * pouvoir cliquer sur les résultats du camembert pour avoir une liste des chatons par pays
   * pages Uptimes (Federation, Organization, Services)
       * des améliorations à faire
           * TODO Cpm visibilité autres liens
           * TODO mrflos (pour l'été) : bidouiller la page statsuptime pour utiliser les filtres par état en js datatables
       * questions de statut manuel vs statut mesuré (page organization)
           * statut manuel seulement
           * statut mesuré seulement
           * les deux
           * un seul combiné des deux
           * discussion :
               * est-ce que la version manuelle est encore utile ? pertinence du mesuré
               * se poser la question de ce que cherche l'utilisateur
               * cas des statuts manuels « en travaux » ou  « fermé »
               * le statut manuel est plus important que le statut mesuré, respecté l'expression des admins
               * ne surtout pas afficher les deux
               * étudier la conjonction
           * TODO Cpm voir pour une version « combinée » avec bulle informative
   * Flo :
       * peut être avoir dans résumé des moyennes sans graphes, genre du texte « sur 2020 XXX visiteurs uniques, YYY ips différentes »
           * Cpm : une notion de « tendance » ?
           * Cpm : donner exemple ?
           * TODO Flo, à réfléchir l'enrichissement de texte des graphes toujours en TODO  > GT le 20 juillet > décalé
           * TODO Flo, à réfléchir à des cadres de tendances dans « Résumé » toujours en  > GT le 20 juillet > décalé
       * changer les intitulés « Web » et « Spécifique » par « Graphes de visites web » ou plus court « Graphes Web » et « Statistiques propres aux services » ou plus court « Stats des services » ?
           * Cpm : préciser l'intention
           * Flo : expliquer les items du menu type > GT le 20 juillet (annulé cause covid...)  > décalé
           * TODO Cpm : ajouter des bulles
           * TODO Flo : tester le menu métriques auprès de personnes
               * en cours toujours en TODO
           * TODO réfléchir
       * TODO Cpm afficher les champs nom et description des métrics dans les diagrammes
   * site statique vs site dynamique ?
       * différence de besoin entre Libre-service.eu (métrique au jour) et le collectif CHATONS (mérique au mois)
       * supprimer les vues années, semaines et jours ?
       * supprimer les périodes 12 mois, 2020 et 2021 ?
       * site statique :
           * avantages : simplicité de déploiement (est-ce nécessaire ?)
           * inconvénients : prend de la place sur disque
       * pour site dynamique :
           * avantages : plus grande interactivité, plus de liberté fonctionnelles
           * inconvénients : nécessite l'installation d'un serveur d'application Java, du travail pour finaliser
       * avis :
           * Antoine : pas de problème au site dynamique
           * Flo : peut-être finir les fonctionnalités en cours avant de faire une version 2 dynamique avec des optimisations
   * Test disponibilité en ipv6 :
       * [https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232](https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232)
       * est-ce une information déclarative ou à calculer ?
           * mieux si c'est calculé car plus à jour et un truc en moins à faire pour les admins
       * sujet non urgent, à approfondir
       * continuer à évaluer les besoins
       * page ipv6 pour avoir un aperçu du point de vue du collectif
   * Vérifier la présence de trackers basic. 
       * [https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232](https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232)
       * oui, très pertinent
       * valorisation : à envisager page dédiée


### Revue générateur de fichiers métrics (statoolinfos)

   * ajout support Jitsi :
       * TODO Cpm en cours
   * ajout support Nextcloud
   * ajout support XMPP
   * metrics http
       * améliorer détection OS TODO Cpm
       * améliorer détection date TODO Cpm
   * pour info: mrflos travaille sur scrumblr et pourra faire la page stat
       * \o/
   * todo mrflos : contacter tobias a propos de l'appli yunohost chatons FAIT le 21 avril
       * ping (19/05/2022) FAIT 
       * réponse tobias
           * Salut,
           * J’ai complètement oublié de te répondre, j’ai presque rien fait, j’ai juste un début de python :
               * #! /usr/bin/python3
               * import sys
               * sys.path.insert(0, « /usr/lib/moulinette/ »)
               * import yunohost
               * yunohost.init\_logging()
               * yunohost.init\_i18n()
               * from yunohost.app import app\_info()from yunohost.app import app\_list()
               * apps = [ app[‹ id ›] for app in app\_list()[‹ apps ›] ]
           * Si vous organiser un repo, je voudrais bien le lien, je pourrais essayer de passer du temps dessus.
       * Créer un dépôt -> Angie le créé > FAIT (16/06)
           * [https://framagit.org/chatons/chatonsinfos-yunohost](https://framagit.org/chatons/chatonsinfos-yunohost)
           * trouver un animateur de ce dépôt (créer, surveiller, répondre etc..) -> voir avec Flo, Tobias ou Aleks
           * TODO CPM faire un appel sur le forum
           * TODO Angie : programmer un atelier au Camp


### Revue des catégories

   *  ([https://stats.chatons.org/category-autres.xhtml)](https://stats.chatons.org/category-autres.xhtml))
   * TODO Angie : ajout du logiciel Garage
       * à rajouter dans Hébergement de sites web / blogs


### Revue des fichiers properties de membres

   * passer en revue :
       *  [https://stats.chatons.org/chatons-crawl.xhtml](https://stats.chatons.org/chatons-crawl.xhtml)
           * plus tard
       * [https://stats.chatons.org/chatons-propertyalerts.xhtml](https://stats.chatons.org/chatons-propertyalerts.xhtml)
           * rouges : 495 ~~500~~ ~~498~~ ~~284~~ ~~289 ~~ ~~292~~  ~~316~~
           * jaunes :  1211 ~~1208~~ ~~1209~~ ~~1220~~  ~~1217~~  ~~1220~~  ~~1226~~ 
           * communiquer un jour prochain (au moment de la prochaine release - celle avec les metrics)
   * Chatons.properties : entrées/sorties, nombre de membre 2022
       * TODO Angie
   * Ajouter les 3 fichiers organization.properties des 3 chatons de la portée 14 manquants
       * TODO angie
   * Anancus
       * [https://stats.chatons.org/anancuslechatonauxlonguesdefenses-propertycheck.xhtml](https://stats.chatons.org/anancuslechatonauxlonguesdefenses-propertycheck.xhtml)
       * fichier properties d'organisation
       * host.server.distribution = YunoHost
       * TODO contacter FAIT via forum, réponse positive
       * FAIT \o/
   * rapprochement avec fiches chatons.org (prospective) :
       * constat qu'il y a eu une convergence de la fiche service sur chatons.org et le fichier properties de service
       * comment pourrait s'envisager une étape supplémentaire de rapprochement ?
       * niveau organisation :
           * si on complète le formulaire de la fiche structure (?) sur chatons.org alors chatons.org serait à même de générer automatiquement le fichier properties de l'organisation
               * Si on automatise à la création d'une fiche, on va se retrouver avec les fiches des candidats chatons et autres huluberlus qui créent des fiches sans jamais candidater. Il faudrait donc que la génération n'intervienne que lorsqu'on modifie la valeur du champ `Etat de la candidature` en `chaton approuvé`    +1 +1
           * avantages :
               * un plus grand nombre d'organisations suivies dans stats.chatons.org
               * une gestion automatique
           * inconvénients :
               * besoin d'étudier des cas (suppression de fiche structure, sortie du collectif, etc.)
       *  niveau service :
           * si on complète le formulaire de la fiche service sur chatons.org alors chatons.org serait à même de générer automatiquement le fichier properties de service
               * comment la génération automatique de ces fiches service.properties viendront-elles alimenter la section Subs de fiches organization.properties ?
                   * très important oui
           * avantages :
               *  un plus grand nombre de services dans stats.chatons.org
               * une gestion automatique
           * besoin d'étudier des cas (suppression de fiche service, sortie du collectif, etc.)
           * Premier pas :
               * Rassembler les infos :
                   * Le nom de l'information et où se trouve-t-elle (si ça se trouve ? et où : fiche Chatons ? Properties) ?
       * TODO recensement comparatif + table de concordance, FAIT Angie \o/
           * [https://asso.framasoft.org/nextcloud/s/3cxYfqAms6Zg6pe](https://asso.framasoft.org/nextcloud/s/3cxYfqAms6Zg6pe)
       * TODO sur les prochaines réunions : définir où intégrer chaque champ de stats.chatons qui n'est pas encore présent sur chatons.org


### Revue des tickets

   * [https://framagit.org/chatons/chatonsinfos/-/issues/1](https://framagit.org/chatons/chatonsinfos/-/issues/1)
       * Redesign des encarts au dessus des tableaux
       * prévu lorsqu'on aura toutes les informations affichées
       * statut : plus tard
   * [https://framagit.org/chatons/chatonsinfos/-/issues/2](https://framagit.org/chatons/chatonsinfos/-/issues/2)
       * Dans le tableau des services de la fiche organisation des chatons, supprimer la colonne "Organisation"
       * Cpm : ce tableau est une vue mutualisée entre plusieurs pages : organisation, services, catégorie,  logiciel ; l'information est effectivement redondante pour la page organisation, mais ça permet de conserver l'homogénéité de la vue.
       * Cpm : pour gagner de la place, possibilité de ne mettre que le logo de l'organisation et le nom en bulle
       * statut : réfléchir et sinon sera traité par la grande revue visuelle prévue un jour
   * [https://framagit.org/chatons/chatonsinfos/-/issues/4](https://framagit.org/chatons/chatonsinfos/-/issues/4)
       * Penser un nouveau fichier properties dédié aux offres non logicielles, celle d'hébergement
       * statut : priorité aux services utilisateurs donc pertinent mais plus tard


### Revue des merge requests

   * [https://framagit.org/framasoft/chatons/-/merge\_requests](https://framagit.org/framasoft/chatons/-/merge\_requests)
   * [https://framagit.org/chatons/chatonsinfos/-/merge\_requests/46](https://framagit.org/chatons/chatonsinfos/-/merge\_requests/46)
       * Ajout du fichier properties de Chalec
       * TODO Cpm FAIT : [https://stats.chatons.org/chalec.xhtml](https://stats.chatons.org/chalec.xhtml)
   * [https://framagit.org/chatons/chatonsinfos/-/merge\_requests/47](https://framagit.org/chatons/chatonsinfos/-/merge\_requests/47)
       * ajout fichier properties DeuxFleurs
       * TODO Cpm FAIT : [https://stats.chatons.org/deuxfleurs.xhtml](https://stats.chatons.org/deuxfleurs.xhtml)


### Revue du forum

   * [https://forum.chatons.org/c/collectif/stats-chatons-org/83](https://forum.chatons.org/c/collectif/stats-chatons-org/83)
       * [https://forum.chatons.org/t/tuto-enregistrer-son-chaton-sur-stats-chatons-org/3957](https://forum.chatons.org/t/tuto-enregistrer-son-chaton-sur-stats-chatons-org/3957)
       * [https://forum.chatons.org/t/brique-camp-yunocamp-2022/3953](https://forum.chatons.org/t/brique-camp-yunocamp-2022/3953)
           * « Ce peut être l’occasion de faire un paquet pour s’interconnecter avec stats.chatons.org. »

### Revue des disponibilités des services (uptimes)

   * [https://stats.chatons.org/chatons-uptimes.xhtml](https://stats.chatons.org/chatons-uptimes.xhtml)
   * Sleto > Equipe
       * rouge depuis au moins 3 semaines
       * TODO Jeey/MrFlos contacter
           * Jeey relance (05/05)
               * pas de réponse
       * TODO Angie relancer FAIT (16/06)
           * à voir au camp CHATONS
   * ~~Roflcopter > Cryptpad~~
       * ~~le service est pourtant accessible~~
       * ~~pb de certificat : curl: (60) SSL certificate problem: unable to get local issuer certificate~~
       * ~~TODO Cpm : approfondir et contacter~~
       * ~~Résolu (21/07)~~
   * Sans-nuage / ARN > Tâches
       * lenteur de réponse donc time out
       * TODO Jeey contacter (16/06)
           * FAIT (16/06)
           * Résolu (17/06)
   * Tchat > Colibris
       * connu, en cours de réinstallation
   * LSTU > NoMagic
       * TODO Jeey le prévenir
           * FAIT (21/07)


### Revue de l'ontologie

   * rappels :
       * l'ordre des questions à se poser : préfixe, sous-préfixe
       * on rajout .count si le mot signifiant est le préfixe
       * métriques HTTP :
           * contexte :
               * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties)
               * [http://www.webalizer.org/webalizer\_help.html](http://www.webalizer.org/webalizer\_help.html)
                   * [https://web.archive.org/web/20210330151216/http://www.webalizer.org/webalizer\_help.html](https://web.archive.org/web/20210330151216/http://www.webalizer.org/webalizer\_help.html)


   * mesure du CO2 :
       * proposer un gdt dédié à ce sujet lors de la prochaine réunion mensuelle
       * TODO Cpm FAIT


   * host.server.distribution
       * pas très propagé
       * ~~TODO Cpm voir le cas anancus~~
       * TODO Cpm faire un rappel forum (faire un nouveau post) ++
       * en profiter pour  :
           * service.registration.load
           * service.install.type
           * indiquer où aller voir


   * ajout de organization.type ?
       * valeur de fiche chatons :
           * ASSOCIATION # Association loi 1901
           * ~~COLLECTIVE~~ vs ~~INFORMALCOLLECTIVE ~~vs INFORMAL # Collectif informel
           * ~~COOPERATIVE COMPANY ~~vs COOPERATIVE
               * COOPERATIVE : entreprise coopérative 
           * ~~TINYCOMPANY~~ vs MICROCOMPANY # Entreprise individuelle (exemples: auto-entreprise, EU…)
           * COMPANY # Entreprise
           * INDIVIDUAL # Individuel
           * OTHER # Autre type d'organisation
       * ligne pour le modèles :
    # Type d'organisation, un parmi {qsdfqsfdqsdf}, obligatoire).

       * homogénéiser : OUI
       * TODO Angie rajouter modèle + changelog FAIT
       * TODO Cpm coder fichier properties
       * TODO Cpm changelog
       * TODO Cpm annonce forum


   * fiche properties hébergement
       * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/hosting.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/hosting.properties)
       * déjà 2 fichiers properties remplis !
       * projection d'intégration dans stats.chatons.org
           * besoin d'un bouton « Services dédiés » 
           * certainement renommer « Services » en « Services mutualisés » +1
           * page d'index : 
               * ajouter un nombre spécifique pour les services dédiés
               * ajouter un indicateur pour différencier
               * dans le tableau : un colonne pour les services dédiés / une pour les services mutualisés
           * page d'un membre :
               * ajouter un nombre spécifique pour les services dédiés
               * ajouter un indicateur pour différencier
           * nommage : service mutualisé vs service dédié
       * TODO intégrer dans le site stats.chatons.org
       * TODO communiquer auprès du collectif sur l'existence de cette nouvelle fiche


   * hosts.os + host.hypervisor :
       * la question se pose également pour la section host normale
           * d'ailleurs le cas vps.* est traitable dans host.*
           * recenser les systèmes d'exploitation des vm et des hosts ?
               * host.os
           * recenser les systèmes d'hypervision des vm et des hosts ?
               * host.hypervisor
           * service.os // hosting.os : compliqué à exprimer / ne correspond pas vraiment
       * on collecte déjà beaucoup d'informations,  est-ce bien nécessaire ?
           * permettrait de faire se questionner sur le logiciel de supervision utilisé
           * pas utile d'utiliser ce champ pour faire du contrôle
           * utile pour savoir voir quel logiciel est utilisé (solidarité)
       * puisque hosts.server.distribution existe alors hosts.os est moins utile
       * donc ajout :
           * *# Nom du logiciel hyperviseur (type STRING, optionnel, ex. KVM).*
           * host.provider.hypervisor = 
       * TODO implémenter STatoolInfos
       * TODO modifier CHANGELOG FAIT


   * prévoir de communiquer sur l'usage possible des stats
